package ng.isec.cardcontrol.admin.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ng.isec.cardcontrol.admin.dao.*;
import ng.isec.cardcontrol.admin.entity.*;

import ng.isec.cardcontrol.admin.service.*;
import ng.isec.cardcontrol.admin.util.AdminUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.client.Client;
//import javax.ws.rs.client.Entity;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
import java.net.URI;
import java.security.Principal;
import java.time.LocalDate;
import java.util.*;

@Controller
@RequestMapping(value = "/app/**")
@ConfigurationProperties(value="api")
public class TransactionController {

    private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
    private static final int INITIAL_PAGE = 0;

    @Value("${api.searchCard}")
    private String searchCardURL;

    @Value("${api.searchCardHost}")
    private String searchCardHost;

    @Value("${api.searchCardURI}")
    private String searchCardURI;

    @Value("${api.serverUrl}")
    private String serverUrl;

    private final RestTemplate restTemplate;

//    private final Client restClient;

    @Autowired
    private IUserInfoDAO userInfoDAO;

    @Autowired
    private CardControlAdminRepository controlAdminRepository;

    @Autowired
    private CardControlAuditRepo cardControlAuditRepo;

    @Autowired
    private CardControlSettingsRepository cardControlSettingsRepo;

    @Autowired
    private CardTransactionRepository cardTransactionRepo;

    @Autowired
    private CardTransactionResponseRepository cardTransactionResponseRepo;

    @Autowired
    private OtherCardTransactionServiceImpl otherCardTransactionService;

    @Autowired
    private OtherCardTransactionRepo otherCardTransactionRepo;

    private final CardAdminRequestsService adminRequestsService;

    private final CardTransactionServiceImpl cardTransactionService;

    private final CardControlSettingsService cardControlSettingsService;

    private final TransactionResponseService transactionResponseService;

    private final AuditService auditService;

    private final AdminUserService cardControlAdminUserService;


    @Autowired
    @Qualifier(value = "passwordEncoder")
    private PasswordEncoder passwordEncoder;

//    @Autowired()
//    @Qualifier("adAuthService")
//    private AuthenticationServiceSoap adServiceSoap;

    private String appKey;

    private String appId;

    @Autowired
    public TransactionController(CardAdminRequestsService adminRequestsService, @Qualifier("searchRestTemplate") RestTemplate restTemplate, CardTransactionServiceImpl cardTransactionService, CardControlSettingsService cardControlSettingsService, TransactionResponseService transactionResponseService, AuditService auditService, AdminUserService cardControlAdminUserService) {
        this.adminRequestsService = adminRequestsService;
        this.restTemplate = restTemplate;
//        this.restClient = restClient;
        this.cardTransactionService = cardTransactionService;
        this.cardControlSettingsService = cardControlSettingsService;
        this.transactionResponseService = transactionResponseService;
        this.auditService = auditService;
        this.cardControlAdminUserService = cardControlAdminUserService;

    }


    @ModelAttribute("channelsList")
    public List<Channel> channelList() {
        return Arrays.asList(Channel.values());
    }

    @ModelAttribute("userRoleList")
    public List<UserRoles> rolesList()
    {
        List<UserRoles> userRolesList = Arrays.asList(UserRoles.values());
        return    userRolesList;

    }




    @Secured({"ROLE_ADMIN", "ROLE_USER","ROLE_CONTROL_CHECKER"})
    @RequestMapping(method = RequestMethod.GET, value = "/viewCardSetting")
    public String viewCardSetting(Model model, Principal principal) {


        CardControlSettings cardControlSettingsDisplay = new CardControlSettings();

//		CardControlSettings cardControlSettings = this.cardControlSettingsRepo.findOne(1);
//
//		cardControlSettings.makeCountryList();

		model.addAttribute("channelsList", Arrays.asList( Channel.values()));
        model.addAttribute("cardSettingBean", cardControlSettingsDisplay);
//		model.addAttribute("cardSettingDisplayBean", cardControlSettings);

        return "cardSetting";
    }


    @Secured({"ROLE_ADMIN", "ROLE_USER","ROLE_CONTROL_CHECKER"})

    @RequestMapping(value = "/viewCardSetting", method = RequestMethod.POST)
    public String processViewCardSetting( Principal principal, HttpServletRequest httpServletRequest, Model model, @ModelAttribute("cardSettingBean") CardControlSettings cardControlSetting
                                          ) {


        if ( (cardControlSetting.getPan() != null) && (cardControlSetting.getCustID() != null)) {

           List<CardControlSettings> cardControlSettingsList = this.cardControlSettingsService.findCardByCustIdAndPan(cardControlSetting.getCustID(),cardControlSetting.getPan());

           if(!cardControlSettingsList.isEmpty()) {
               cardControlSettingsList.stream().forEach((cardControlSettings -> {
                   cardControlSettings.makeCountryList();
               }));

               model.addAttribute("cards", cardControlSettingsList);
               model.addAttribute("cardSettingBean", cardControlSetting);
               model.addAttribute("search", true);
           }else
               {
                   return "redirect:/app/viewCardSetting";
               }

//         CardControlSettings cardControlSetting = this.cardControlSettingsRepo.findByPan(pan);
//
//         if (cardControlSetting != null) {
//             cardControlSetting.makeCountryList();
//             model.addAttribute("cardSettingBean", cardControlSetting);
//          }

        } else {
            return "redirect:/app/viewCardSetting";
        }

        return "cardSetting";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/editSettings", method = RequestMethod.GET)
    public String editCardSetting(@RequestParam(required = false, name = "cardId") Integer cardId, HttpServletRequest httpServletRequest, Model model) throws JsonProcessingException {

        CardControlSettings cardControlSettings = this.cardControlSettingsRepo.getOne(cardId);
        cardControlSettings.makeCountryList();

        model.addAttribute("editCardSettingBean", cardControlSettings);
        return "editCardSetting";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/editSettings", method = RequestMethod.POST)
    public String processEditSetting(@ModelAttribute("editCardSettingBean") CardControlSettings cardControlSettings, BindingResult bindingResult, HttpServletRequest httpServletRequest, Model model) throws JsonProcessingException {

        Integer cardControlSettingsId = cardControlSettings.getId();
        logger.info("Bound Card Setting: " + cardControlSettings.getId() + " " + cardControlSettings.getDailyLimit() + " " + cardControlSettings.getMonthlyLimit() + " " + cardControlSettingsId + " " + cardControlSettings.getCountries());

        if (bindingResult.hasErrors()) {
            return "redirect:/editSettings?cardId=" + cardControlSettingsId;
        }

        ObjectMapper mapper = new ObjectMapper();

        String allowedCountriesJSON = mapper.writeValueAsString(cardControlSettings.getCountries());

        logger.info("Allowed Countries JSON: " + allowedCountriesJSON);

        this.cardControlSettingsRepo.save(cardControlSettings);
        return "redirect:/app/editSettings?cardId=" + cardControlSettingsId + "&success";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/viewAllCards")
    public String viewAllCards(Model model) {
        return "allCards";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/viewTrxResponse", method = RequestMethod.GET)
    public String viewTransactionResponse(@RequestParam(value = "rrnNo", defaultValue = "") String rrnNumber,
                                          @RequestParam(value = "pan", defaultValue = "") String pan,
                                          @RequestParam(value = "acctNo", defaultValue = "") String acctNumber,
                                          @RequestParam(value = "stan", defaultValue = "") String stan,
                                          Model model) {

        if( rrnNumber!= null && (pan.isEmpty()))
        {
            CardTransactionResponse cardTransactionResponse = this.transactionResponseService.getTransactionResponse( rrnNumber );

            if (cardTransactionResponse != null) {
                model.addAttribute("cardTrxnResponseBean", Arrays.asList(cardTransactionResponse));
            }
            return "trxResponse";
        }

        if ((rrnNumber != null) && (pan != null) && (acctNumber != null)) {
            CardTransactionResponse cardTransactionResponse = this.transactionResponseService.getTransactionResponse(rrnNumber, stan, pan);

            if (cardTransactionResponse != null) {
                model.addAttribute("cardTrxnResponseBean", Arrays.asList( cardTransactionResponse));
            }
        }

        return "trxResponse";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER","ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER","ROLE_CONTACT_CENTER_MAKER","ROLE_CONTACT_CENTER_CHECKER","ROLE_SUPPORT"})

    @RequestMapping(value = "/cardTrxn", method = RequestMethod.GET)
    public String cardTrxn(Model model, Principal principal) {

        model.addAttribute("cardTransaction", new CardTransaction());
        return "cardTrxn";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER","ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER","ROLE_CONTACT_CENTER_MAKER","ROLE_CONTACT_CENTER_CHECKER","ROLE_SUPPORT"})

    @RequestMapping(value = "/cardTrxn", method = RequestMethod.POST)
    public String processViewCardTrxn(@ModelAttribute("cardTransaction") CardTransaction cardTransaction,
                                      BindingResult bindingResult,
                                      Model model, Principal principal) {

        String acctNumber = cardTransaction.getAcctNumber();
        String pan = cardTransaction.getPan();

        if ((acctNumber != null) && (pan != null)) {

            List<CardTransaction> cardTransactionList = this.cardTransactionService.getCardTranx( acctNumber,pan );

            if(!cardTransactionList.isEmpty())
            {
                model.addAttribute("cardTransactionList", cardTransactionList);
                model.addAttribute("cardTransaction", cardTransaction);
                model.addAttribute("search", true);

            }else
            {
                return "redirect:/app/cardTrxn";
            }
        }

        return "cardTrxn";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER","ROLE_SUPPORT"})

    @RequestMapping(value = "/viewTrxns")
    public String viewAllTrxn( Model model , Principal principal) {


//		Page<CardTransaction> cardTransactions = this.cardTransactionRepo.findAll(this.gotoPage(evalPage, "updatedDate"));
//		Pager pager = new Pager(cardTransactions.getTotalPages(),cardTransactions.getNumber(), 5);
//        List<CardTransaction> cardTransactionList = this.cardTransactionRepo.findTopAll();

        List<CardTransaction> cardTransactionList = this.cardTransactionService.getTopTrnx();

        model.addAttribute("transactions", cardTransactionList);
//		model.addAttribute("pager",pager);

        return "allCardTrxn";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER","ROLE_SUPPORT"})
    @RequestMapping(value = "/viewTrxns", method = RequestMethod.POST)
    public String processViewAllTrxn(Principal principal) {

        return "allCardTrxn";
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping(path = "/app/newUser")
    public String createNewUser(CardControlAdminUser controlAdminUserForm, @RequestParam(value = "uname", required = false) String uName, HttpServletRequest httpServletRequest, Model model, Principal principal) {

       List<UserRoles> userRolesList = Arrays.asList(UserRoles.values());

        CardControlAdminUser cardControlAdminUser = new CardControlAdminUser();
        model.addAttribute("adminUser", cardControlAdminUser);
        model.addAttribute("userRoleList",userRolesList);

        return "newAdminUser";
    }

    @Secured({"ROLE_ADMIN","ROLE_CONTROL_MAKER"})
    @PostMapping(value = "/newUser")
    public String processNewUser(Principal principal, HttpServletRequest httpServletRequest, @Valid() @ModelAttribute("adminUser") CardControlAdminUser cardControlAdminUser, BindingResult bindingResult) {

//        if (bindingResult.hasErrors()) {
//            redirectAttributes.addFlashAttribute("errorMessage", "Could not create user.");
//            return "redirect:/app/newUser";
//
//        }

//        String adUsername = this.adServiceSoap.getStaffIDWithUserName(cardControlAdminUser.getUsername(),appId,appKey);
//        logger.info("New User AD username. "+adUsername);
//
//        if(adUsername.contains("Access Denied") )
//        {
//            bindingResult.rejectValue("username","username","Not A Valid AD User.");
//        }
//
        Optional<CardControlAdminUser> adminUserOptional = this.cardControlAdminUserService.getCardControlUserByUsername(cardControlAdminUser.getUsername());

        if (adminUserOptional.isPresent())
        {
            bindingResult.rejectValue("username","username","Username Already Exists. Please Choose A New Username");
        }

        if(bindingResult.hasErrors())
        {
            return "newAdminUser";

        }
        this.createNewUser(cardControlAdminUser);
        return "redirect:/app/newUser?success";
    }


    @PostMapping(path="/app/processNewUser")
    @Secured({"ROLE_CONTROL_MAKER","ROLE_ADMIN"})
    public String processNewUserRequest( @Valid @ModelAttribute("adminUser") CardControlAdminUser controlAdminUserForm, BindingResult bindingResult, HttpServletRequest httpServletRequest,Model model, Principal principal)
    {

        Optional<CardControlAdminUser> adminUserOptional = this.cardControlAdminUserService.getCardControlUserByUsername(controlAdminUserForm.getUsername());

        if (adminUserOptional.isPresent())
        {
            bindingResult.rejectValue("username","username","Username Already Exists. Please Choose A New Username");
        }

        if(bindingResult.hasErrors())
        {
            return "newAdminUser";

        }
        CardControlAdminUser cardControlAdminUser = this.createNewUser(controlAdminUserForm);

        CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
        controlAdminRequests.setInitiator(principal.getName());
        controlAdminRequests.setApproved(false);
        controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
        controlAdminRequests.setRequestType(RequestType.CREATE_USER.name());


        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );
        cardControlAdminRequestDetail.setUsername(controlAdminUserForm.getUsername());
        cardControlAdminRequestDetail.setUserRole(controlAdminUserForm.getRole());
        cardControlAdminRequestDetail.setEnabled(false);

        controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
        cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);
        cardControlAdminRequestDetail.setUserId(cardControlAdminUser.getId());

        this.adminRequestsService.saveRequest( controlAdminRequests);

        return "redirect:/app/newUser?success";
    }




    @Secured({"ROLE_ADMIN","ROLE_USER", "ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER"})
    @GetMapping(value="/users")
    public String manageUser(HttpServletRequest httpServletRequest, Model model){

        List<CardControlAdminUser> cardControlAdminUsers = this.cardControlAdminUserService.getAllUsers();
        cardControlAdminUsers.stream().forEach((controlAdminUser)->{controlAdminUser.setRole(UserRoles.valueOf(controlAdminUser.getRole()).getRoleName());});

        model.addAttribute("usersList",cardControlAdminUsers);

        return "users";
    }

    @Secured({"ROLE_ADMIN","ROLE_USER","ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER"})
    @RequestMapping(value="/editUser", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "usrName",required = false) String userName,
                           @RequestParam(value = "uId", required = false) Integer uId,
                           HttpServletRequest httpServletRequest, Model model)
    {
        Optional<CardControlAdminUser> cardControlAdminUser = Optional.empty();
        Optional<Integer> id = Optional.ofNullable(uId);
        cardControlAdminUser = this.cardControlAdminUserService.getCardControlUser(id.get());
        CardControlAdminUser controlAdminUser = cardControlAdminUser.get();
        controlAdminUser.setAccountEnabled(controlAdminUser.getEnabled() == 1);


//        if(uId != null )
//        {
//           cardControlAdminUser = this.cardControlAdminUserService.getCardControlUser(uId);
//            controlAdminUser = cardControlAdminUser.get();
//            controlAdminUser.setAccountEnabled(controlAdminUser.getEnabled()== 1);
//        }

        model.addAttribute("editCardAdminUser", controlAdminUser);
        return "editUser";
    }


    @PostMapping(path="/app/processEditUser")
    @Secured({"ROLE_CONTROL_MAKER","ROLE_ADMIN"})
    public String processEditUserRequest(Principal principal, HttpServletRequest httpServletRequest, @ModelAttribute("editCardAdminUser") CardControlAdminUser controlAdminUser, BindingResult bindingResult,Model model )
    {


        CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
        controlAdminRequests.setInitiator(principal.getName());
        controlAdminRequests.setApproved(false);
        controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
        controlAdminRequests.setRequestType(RequestType.EDIT_USER.name());


        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );
        cardControlAdminRequestDetail.setUsername(controlAdminUser.getUsername());
        cardControlAdminRequestDetail.setUserRole(controlAdminUser.getRole());
        cardControlAdminRequestDetail.setEnabled((controlAdminUser.isAccountEnabled()));
        cardControlAdminRequestDetail.setUserId(controlAdminUser.getId());


        controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
        cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);

        this.adminRequestsService.saveRequest( controlAdminRequests);

        return "redirect:/app/editUser?uId="+controlAdminUser.getId()+"&usrName="+controlAdminUser.getUsername()+"&saved";
    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @PostMapping(path = "/editUser")
    public String processEditUser(@Valid() @ModelAttribute(name="editCardAdminUser") CardControlAdminUser cardControlAdminUser, BindingResult bindingResult, HttpServletRequest httpServletRequest)
    {
        if(bindingResult.hasErrors())
        {
            return "editUser";
        }
        cardControlAdminUser.setId(cardControlAdminUser.getId());
        cardControlAdminUser.setEnabled((cardControlAdminUser.isAccountEnabled() )? 1: 0);
        cardControlAdminUser.setRole((Enum.valueOf(UserRoles.class, cardControlAdminUser.getRole()).name()));

        this.controlAdminRepository.save(cardControlAdminUser);

        return "redirect:/app/editUser?uId="+cardControlAdminUser.getId()+"&usrName="+cardControlAdminUser.getUsername()+"&saved";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/viewAudit", method = RequestMethod.GET)
    public String viewAudit(Model model) {
        List<CardControlAudit> auditList = this.auditService.getAllAudit();
        model.addAttribute("auditActions", auditList);

        return "auditTrails";
    }
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(value="/otherTrx")
    public String viewOtherTrxn(HttpServletRequest httpServletRequest, Model model)
    {
        model.addAttribute("otherCardTrxn",new OtherCardTransaction());
//        List<OtherCardTransaction> transactionList = this.otherCardTransactionService.getAllOtherTransactions();
//        model.addAttribute("transactions",transactionList);

        return "allCards";
    }

    private CardControlAdminUser createNewUser(CardControlAdminUser controlAdminUser) {
        CardControlAdminUser cardControlAdminUser = controlAdminUser;

        logger.info(""+ controlAdminUser.getRole());
        UserRoles  role = Enum.valueOf(UserRoles.class,controlAdminUser.getRole());
        logger.info("Selected Role. "+ role.name());
        String passwd = cardControlAdminUser.getPassword();
        logger.info(""+passwd.trim());
        cardControlAdminUser.setRole((UserRoles.valueOf( controlAdminUser.getRole())).name());
        cardControlAdminUser.setUsername(controlAdminUser.getUsername());
        cardControlAdminUser.setEnabled(AdminUtil.USER_DISABLED.shortValue());
        cardControlAdminUser.setPassword(this.passwordEncoder.encode(cardControlAdminUser.getPassword()));

//        this.controlAdminRepository.save(cardControlAdminUser);
        this.userInfoDAO.saveUser(cardControlAdminUser);
        return cardControlAdminUser;

    }



    @Secured({"ROLE_ADMIN","ROLE_SUPPORT","ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER"})
    @GetMapping(value="/app/searchCard")
    public String searchCard(HttpServletRequest httpServletRequest, Model model)
    {
        return "searchCard";
    }

    @Secured({"ROLE_ADMIN","ROLE_SUPPORT","ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER"})
    @GetMapping(path="/app/processSearchCard")
    public String processSearchCard(@RequestParam("acctNumber") String acctNumber,
                                    HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    Model model){

        CardSettingsDTO cardSettingsDTO = new CardSettingsDTO();
        cardSettingsDTO.setAccountNo(acctNumber);

        CardDetail[] response = this.restTemplate.postForObject(URI.create(searchCardURL),cardSettingsDTO, CardDetail[].class);
        List<CardDetail> list = Arrays.asList(response);

//        Response response1 = this.restClient.target(URI.create(searchCardURL)).request(MediaType.APPLICATION_JSON).post(Entity.entity(cardSettingsDTO, MediaType.APPLICATION_JSON));
//        CardDetail[] cardDetail = response1.readEntity(CardDetail[].class);
//        List<CardDetail> list = Arrays.asList(cardDetail);
//
//        logger.info( "Response: "+cardDetail.length+" Status: "+response1.getStatus());

        model.addAttribute("cards", list);
        model.addAttribute("acctNumber", acctNumber);
       return "searchCard";
//       return "chooseCard";
    }
    @Secured({"ROLE_ADMIN"})
    @GetMapping("/app/form")
    public String showForm(CardControlAdminUser personForm, Model model) {
        model.addAttribute("person",new CardControlAdminUser());
        return "form";
    }

    @Secured({"ROLE_CONTROL_MAKER","ROLE_ADMIN"})
    @PostMapping("/app/form")
    public String checkPersonInfo(@Valid @ModelAttribute("person") CardControlAdminUser controlAdminUserForm, BindingResult bindingResult, Model model) {

        Optional<CardControlAdminUser> adminUserOptional = this.cardControlAdminUserService.getCardControlUserByUsername(controlAdminUserForm.getUsername());

        if (adminUserOptional.isPresent())
        {
            bindingResult.rejectValue("username","username","Username Already Exists. Please Choose A New Username");
        }

        if (bindingResult.hasErrors()) {
            return "form";
        }

        return "redirect:/results";
    }

    private PageRequest gotoPage(int page, String sortedField) {
        PageRequest pageRequest = new PageRequest(page, 15, Sort.Direction.DESC, sortedField);

        return pageRequest;
    }

    @ModelAttribute(name="currentYear")
    public String getCurrentDate()
    {
        return String.format("%s", LocalDate.now().getYear());
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

}
