package ng.isec.cardcontrol.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {
	
	@RequestMapping(method = RequestMethod.GET,value={"/","/login"})
	public String index( Model model )
	{
		return "home";
	}

	@RequestMapping(method = RequestMethod.GET,value={"/login_error"})
	public String loginError(Model model, HttpServletRequest httpServletRequest)
	{
		model.addAttribute("error",true);
		return "report";
	}

	
}
