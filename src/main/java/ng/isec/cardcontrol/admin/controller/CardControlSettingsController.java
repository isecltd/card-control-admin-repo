package ng.isec.cardcontrol.admin.controller;

import ng.isec.cardcontrol.admin.dao.CardControlSettingsRepository;
import ng.isec.cardcontrol.admin.entity.*;
import ng.isec.cardcontrol.admin.service.AdminRequestDetailService;
import ng.isec.cardcontrol.admin.service.CardAdminRequestsService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.math.BigDecimal;
import java.net.URI;
import java.security.Principal;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by isec on 06/12/2018.
 */
@Controller
@PropertySource(value="classpath:application.properties")
@ConfigurationProperties(value="api")
public class CardControlSettingsController {

    private final Logger logger = org.slf4j.LoggerFactory.getLogger(CardControlSettingsController.class);

    @Value("${api.addCard}")
    private String addCard;

    @Value("${api.getCardSetting}")
    private String getCard;


    @Value("${api.lockCard}")
    private String lockCard;

    @Value("${api.updateCountries}")
    private String updateCountries;

    @Value("${api.dailyLimit}")
    private String dailyLimit;

    @Value("${api.monthlyLimit}")
    private String monthlyLimit;

    @Value("${api.serverUrl}")
    private String serverUrl;

    @Value("${api.port}")
    private String serverPort;

    @ModelAttribute("channelsList")
    public List<Channel> channelList() {
        return Arrays.asList(Channel.values());
    }

    private final List<String> isoCountriesList = getIsoCountriesList();
    private final List<String> countryNames = this.getCountries();

    private final RestTemplate restTemplate;

    private final CardControlSettingsRepository controlSettingsRepository;

    private final CardAdminRequestsService adminRequestsService;


    @Autowired
    public CardControlSettingsController(@Qualifier("cardControlRestTemplate") RestTemplate restTemplate, CardControlSettingsRepository cardControlSettingsRepository, CardAdminRequestsService adminRequestsService) {
        this.restTemplate = restTemplate;
        this.controlSettingsRepository = cardControlSettingsRepository;
        this.adminRequestsService = adminRequestsService;
    }

    @GetMapping(path = "/app/controlCard")
    @Secured({"ROLE_ADMIN","ROLE_SUPPORT","ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER"})

    public String cardSettings(@RequestParam(name="cardId", required = false) String cardId, HttpServletRequest httpServletRequest, Model model)
    {
//        CardControlSettings controlSettings = null;
//
//        if(cardId != null )
//        {
//            controlSettings  = this.controlSettingsRepository.findByCustID(cardId);
//            controlSettings = (controlSettings == null) ? new CardControlSettings(): controlSettings;
//        }
//
//        model.addAttribute("cardSetting", controlSettings);

        return "controlCard";
    }

    @PostMapping(path="/app/controlCard")
    @Secured({"ROLE_ADMIN","ROLE_SUPPORT","ROLE_CONTROL_MAKER","ROLE_CONTROL_CHECKER"})
    public String processCardSetting(HttpServletRequest httpServletRequest, @ModelAttribute(name="cardSetting") CardControlSettings controlSettings)
    {
        this.restTemplate.postForObject(serverUrl,controlSettings, null);
        return "redirect:/controlCard";
    }

    @GetMapping(path="/app/addCard")
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})
    public String addCard( @RequestParam(value ="custID",required = false) String custId,
                           @RequestParam(name="maskedPan",required = false) String maskedPan,
                           @RequestParam(name="acctNo",required = false) String acctNo,
                           @RequestParam(name="message",required = false) String message,
                           @RequestParam(name="encryptedPan",required = false) String encryptedPan,
                           Model model,HttpServletRequest httpServletRequest)
    {
        if( message != null )
        {
            model.addAttribute("message",message);
        }


        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail();
        CardControlAdminRequests cardControlAdminRequests = new CardControlAdminRequests();
        cardControlAdminRequestDetail.setMaskedPan(maskedPan);
        cardControlAdminRequestDetail.setEncryptedFullPan(encryptedPan);
        cardControlAdminRequestDetail.setCustID(acctNo);
        cardControlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);


        CardControlSettings controlSettings = new CardControlSettings();

        CardSettingsDTO cardSettingsDTO = new CardSettingsDTO();

        cardSettingsDTO.setCustID(custId);
        cardSettingsDTO.setAcctNumber(acctNo);
        cardSettingsDTO.setIsoCountriesList(isoCountriesList);
        cardSettingsDTO.setChannelList(Arrays.asList(Channel.values()));
        cardSettingsDTO.setCountryNames(countryNames);


        List<Country> countryList = new ArrayList<>();
        Map<String, List<Channel>> allowedCountriesMap = new HashMap<>();

        isoCountriesList.stream().forEach((iso)->
        {
            Country country = new Country();
            country.setTwoDigitCode(iso);
            country.setCountryName( new Locale("",iso).getDisplayCountry());
            country.setAllowedTransactionChannels(Arrays.asList(Channel.values()));
            countryList.add(country);
            allowedCountriesMap.put(new Locale("",iso).getDisplayCountry(),Arrays.asList(Channel.values()));
        });

        cardSettingsDTO.setCountriesList(countryList);
        cardSettingsDTO.setAllowedCountriesChannelsMap(allowedCountriesMap);

        model.addAttribute("newCard",cardSettingsDTO);
        model.addAttribute("cardSetting",controlSettings);
        model.addAttribute("cardSettingRequest",cardControlAdminRequests );

        return "controlCard";
//        return "setupCard";
    }
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})
    @GetMapping(path="/app/editCard")
    public String editCard(@RequestParam(value ="custID",required = false) String custId,
                           @RequestParam(name="maskedPan",required = false) String maskedPan,
                           @RequestParam(name="acctNo",required = false) String acctNo,
                           @RequestParam(name="encryptedPan",required = false) String encryptedPan,
                           Model model,HttpServletRequest httpServletRequest){

        CardControlAdminRequests adminRequest = this.adminRequestsService.getCardControlAdminRequest(92);
        CardControlAdminRequestDetail cardControlAdminRequestDetail = adminRequest.getRequestDetail();
        cardControlAdminRequestDetail.makeCountryList();

        CardSettingsDTO cardSettingsDTO = new CardSettingsDTO();
        cardSettingsDTO.setEncryptedFullPan(encryptedPan);
        cardSettingsDTO.setMaskedPan(maskedPan);
        cardSettingsDTO.setCustID(custId);
        cardSettingsDTO.setCountriesList(cardControlAdminRequestDetail.getCountriesList());
        cardSettingsDTO.setDailyLimit(BigDecimal.valueOf( cardControlAdminRequestDetail.getDailyLimit()));
        cardSettingsDTO.setMonthlyLimit(BigDecimal.valueOf( cardControlAdminRequestDetail.getMonthlyLimit()));

        List<String> chosenCountries = new ArrayList<>();
        cardControlAdminRequestDetail.getCountriesList().stream().forEach((e)->{
            chosenCountries.add( e.getCountryName());
        });

        cardSettingsDTO.setCountryNames(chosenCountries);

//        ResponseEntity<CardSettingsDTO> apiResponse = this.restTemplate.postForEntity(URI.create(this.getCard),cardSettingsDTO, CardSettingsDTO.class);
//        model.addAttribute("editCardSettingBean",apiResponse.getBody());




        model.addAttribute("editCardSettingBean",cardSettingsDTO);

        return "editCardSetting";
    }

    @PostMapping(path = "/app/addCard")
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})
    public String processAddCard(@ModelAttribute("cardSettingRequest") CardControlAdminRequests cardControlAdminRequests, HttpServletRequest httpServletRequest, Principal principal)
    {

        CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
        controlAdminRequests.setInitiator(principal.getName());
        controlAdminRequests.setApproved(false);
        controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
        controlAdminRequests.setRequestType(RequestType.CREATE_CARD.name());


        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );
        cardControlAdminRequestDetail.setBlocked(cardControlAdminRequests.getRequestDetail().isBlocked());
        cardControlAdminRequestDetail.setEncryptedFullPan(cardControlAdminRequests.getRequestDetail().getEncryptedFullPan());
        cardControlAdminRequestDetail.setCustID(cardControlAdminRequests.getRequestDetail().getCustID());

        controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
        cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);

        this.adminRequestsService.saveRequest( controlAdminRequests);

//        ApiResponse apiResponse = this.restTemplate.postForObject(URI.create(serverUrl+this.addCard),controlSettings, ApiResponse.class);

//        String message = apiResponse.getMessage();

//        return "redirect:/app/addCard?message="+message;
        return "redirect:/app/addCard";
    }

    @PostMapping(path="/app/lockCard")
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})

    public String processLockCard(@ModelAttribute("cardSettingRequest") CardControlAdminRequests cardControlAdminRequests, HttpServletRequest httpServletRequest, Model model, Principal principal)
    {

        CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
        controlAdminRequests.setInitiator(principal.getName());
        controlAdminRequests.setApproved(false);
        controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
        controlAdminRequests.setRequestType(RequestType.LOCK_CARD.name());


        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );
        cardControlAdminRequestDetail.setBlocked(cardControlAdminRequests.getRequestDetail().isBlocked());

        controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
        cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);

        this.adminRequestsService.saveRequest( controlAdminRequests);


//        ApiResponse apiResponse = this.restTemplate.postForObject(lockCard,controlSettings, ApiResponse.class);
//        String message = apiResponse.getMessage();
//
//        return "redirect:/app/addCard?message="+message;
        return "redirect:/app/addCard";
    }

    @PostMapping(path = "/app/dailyLimit")
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})

    public String processDailyLimit(@ModelAttribute("cardSettingRequest") CardControlAdminRequests cardControlAdminRequests, HttpServletRequest httpServletRequest, Model model, Principal principal)
    {
        CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
        controlAdminRequests.setInitiator(principal.getName());
        controlAdminRequests.setApproved(false);
        controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
        controlAdminRequests.setRequestType(RequestType.DAILY_LIMIT.name());


        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );
        cardControlAdminRequestDetail.setDailyLimit(cardControlAdminRequests.getRequestDetail().getDailyLimit());

        controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
        cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);

        this.adminRequestsService.saveRequest( controlAdminRequests);

//        ApiResponse apiResponse = this.restTemplate.postForObject(dailyLimit,controlSettings, ApiResponse.class);
//        String message = apiResponse.getMessage();
//
//        return "redirect:/app/addCard?message="+message;
        return "redirect:/app/addCard";
    }

    @PostMapping(path="/app/monthlyLimit")
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})

    public String processMonthlyLimit( @ModelAttribute("cardSettingRequest") CardControlAdminRequests cardControlAdminRequests, HttpServletRequest httpServletRequest,Model model, Principal principal)
    {
        CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
        controlAdminRequests.setInitiator(principal.getName());
        controlAdminRequests.setApproved(false);
        controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
        controlAdminRequests.setRequestType(RequestType.MONTHLY_LIMIT.name());


        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );
        cardControlAdminRequestDetail.setMonthlyLimit(cardControlAdminRequests.getRequestDetail().getMonthlyLimit());

        controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
        cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);

        this.adminRequestsService.saveRequest( controlAdminRequests);
        //
//        ApiResponse apiResponse = this.restTemplate.postForObject(monthlyLimit,controlSettings, ApiResponse.class);
//        String message = apiResponse.getMessage();
//
//        return "redirect:/app/addCard?message="+message;

        return "redirect:/app/addCard";
    }

    @GetMapping(path="/selectCard")
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})
    public String processSearchCardResults( HttpServletRequest httpServletRequest){
        return "choseCard";
    }

//    @PostMapping(path = "/app/allowedChannels")
//    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})
//    public String processAllowedChannels(@ModelAttribute("newCard") CardSettingsDTO cardSettingsDTO, BindingResult bindingResult, HttpServletRequest httpServletRequest, Model model, Principal principal)
//    {
//
//        CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
//        controlAdminRequests.setInitiator(principal.getName());
//        controlAdminRequests.setApproved(false);
//        controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
//        controlAdminRequests.setRequestType(RequestType.ALLOWED_COUNTRIES.name());
//
//
//        CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );
//
//        controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
//        cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);
//
//        this.adminRequestsService.saveRequest( controlAdminRequests);
////        ApiResponse apiResponse = this.restTemplate.postForObject(updateCountries,controlSettings, ApiResponse.class);
////        String message = apiResponse.getMessage();
//        logger.info(""+cardSettingsDTO.getMycountries().length);
//        Stream.of(cardSettingsDTO.getMycountries()).forEach((e)->{
//            logger.info(""+e);
//        });
//        String message = "";
//        return "redirect:/app/addCard?message="+message;
//    }

    public String getLockCard() {
        return lockCard;
    }

    public String getUpdateCountries() {
        return updateCountries;
    }

    public String getDailyLimit() {
        return dailyLimit;
    }

    public String getMonthlyLimit() {
        return monthlyLimit;
    }

    public String getAddCard() {
        return addCard;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    private List<String> getCountries()
    {
        List<String> countriesList = new ArrayList<>();

        isoCountriesList.stream().forEach((code)->
        {
            countriesList.add( new Locale("",code).getDisplayCountry());
        });
//        Collections.sort(countriesList);
        return countriesList;
    }
    private List<String> getIsoCountriesList()
    {
        List<String> isoList = Arrays.asList(Locale.getISOCountries());
        Collections.sort(isoList);
        return isoList;
    }

    @ModelAttribute("countries")
    public String[] getAll(){
        List<Country> countryList = new ArrayList<>();
        isoCountriesList.stream().forEach((iso)->
        {
            Country country = new Country();
            country.setTwoDigitCode(iso);
            country.setCountryName( new Locale("",iso).getDisplayCountry());
            country.setAllowedTransactionChannels(Arrays.asList(Channel.values()));
            countryList.add(country);
        });


        String[] dest = getCountries().stream().toArray(String[]::new);
        return dest;
    }

    @ModelAttribute("codes")
    public List<String> getCodes(){
        new Locale("").toLanguageTag();
        List<String> isoList = Arrays.asList(Locale.getISOCountries());
        Collections.sort(isoList);
        String[] dest = isoList.stream().toArray(String[]::new);
        return isoList;


    }
}
