package ng.isec.cardcontrol.admin.controller;

import ng.isec.cardcontrol.admin.entity.*;
import ng.isec.cardcontrol.admin.service.AuditService;
import ng.isec.cardcontrol.admin.service.CardAdminRequestsService;
import ng.isec.cardcontrol.admin.service.OtherCardTransactionService;
import ng.isec.cardcontrol.admin.util.AdminUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
public class RestSearchOtherTransactionController {

    private final Logger logger = LoggerFactory.getLogger(RestSearchOtherTransactionController.class);

    private final OtherCardTransactionService otherCardTransactionService;

    private final CardAdminRequestsService adminRequestsService;

    private final AuditService auditService;

    @Autowired
    public RestSearchOtherTransactionController(OtherCardTransactionService otherCardTransactionService, CardAdminRequestsService adminRequestsService, AuditService auditService) {
        this.otherCardTransactionService = otherCardTransactionService;
        this.adminRequestsService = adminRequestsService;
        this.auditService = auditService;
    }


    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(value="/app/otherTrxSearch")
    public List<OtherCardTransaction> processSearchTrxn(@RequestParam(value = "custID", required = false) String custID,  HttpServletRequest httpServletRequest)
    {
        if(custID != null )
        {
            List<OtherCardTransaction> transactionByPan = this.otherCardTransactionService.getOtherTransactionByPan(custID);
            return transactionByPan;
        }
        List<OtherCardTransaction> streamAll = otherCardTransactionService.getAllOtherTransactions();

        CardControlAudit cardControlAudit= new CardControlAudit();
        cardControlAudit.setIpAddress( httpServletRequest.getRemoteAddr());
        cardControlAudit.setHostname( httpServletRequest.getRemoteHost());

        return streamAll;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping(value="/app/otherTrxSearchByDate")
    public List<OtherCardTransaction> processSearchByDate(@RequestBody() OtherTrxnSearchRequest otherTrxnSearchRequest,
                                                          HttpServletRequest httpServletRequest){

        String searchFrom = otherTrxnSearchRequest.getSearchDateFrom();
        String  searchTo = otherTrxnSearchRequest.getSearchDateTo();
        String custID = otherTrxnSearchRequest.getCustID();

        if(custID != null &&(searchFrom.isEmpty() || searchTo.isEmpty()))
        {
//            logger.info("Executing This Block: Only CustId");
            List<OtherCardTransaction> transactionByPan = this.otherCardTransactionService.getOtherTransactionByPan(custID);
            return transactionByPan;
        }


        if(( custID != null && !(custID.isEmpty()) && !searchFrom.isEmpty() && !searchTo.isEmpty()))
        {
//            logger.info("Executing This Block: All Options.");


            Date dateFrom = AdminUtil.strToDateConvert(searchFrom);
            Date dateTo = AdminUtil.strToDateConvert(searchTo);

            List<OtherCardTransaction> otherCardTransactionList = this.otherCardTransactionService.getOtherTransactionByCustIDDateRange(custID,dateFrom,dateTo);
            return otherCardTransactionList;
        }

//        logger.info("Skipping The Conditions.");

        Date dateFrom = AdminUtil.strToDateConvert(searchFrom.isEmpty()? LocalDate.now().toString():searchFrom);
        Date dateTo = AdminUtil.strToDateConvert(searchTo.isEmpty()?LocalDate.now().toString(): searchTo );

        List<OtherCardTransaction> otherCardTransactionList = this.otherCardTransactionService.getOtherTransactionByDateRange(dateFrom,dateTo);

        return otherCardTransactionList;


    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping(value="/app/auditByDate")
    public List<CardControlAudit> processSearchAuditByDate(@RequestBody() OtherTrxnSearchRequest otherTrxnSearchRequest,
                                                          HttpServletRequest httpServletRequest){

        String searchFrom = otherTrxnSearchRequest.getSearchDateFrom();
        String  searchTo = otherTrxnSearchRequest.getSearchDateTo();

        if(( !searchFrom.isEmpty() && !searchTo.isEmpty()))
        {
            Date dateFrom = AdminUtil.strToDateConvert(searchFrom);
            Date dateTo = AdminUtil.strToDateConvert(searchTo);

            List<CardControlAudit> auditList = this.auditService.getAuditByDateRange(dateFrom,dateTo);
            return auditList;
        }

        Date dateFrom = AdminUtil.strToDateConvert(searchFrom.isEmpty()? LocalDate.now().toString():searchFrom);
        Date dateTo = AdminUtil.strToDateConvert(searchTo.isEmpty()?LocalDate.now().toString(): searchTo );

        List<CardControlAudit> auditList = this.auditService.getAuditByDateRange(dateFrom,dateTo);

        return auditList;


    }

        @PostMapping(path = "/app/allowedCountries")
        @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_MAKER"})
        public String processAllowedChannels(Principal principal,
                                             @RequestBody() OtherTrxnSearchRequest otherTrxnSearchRequest,
                                             HttpServletRequest httpServletRequest,
                                             BindingResult bindingResult, Model model ) {

            CardControlAdminRequests controlAdminRequests = new CardControlAdminRequests();
            controlAdminRequests.setInitiator(principal.getName());
            controlAdminRequests.setApproved(false);
            controlAdminRequests.setRequestDate(Calendar.getInstance().getTime());
            controlAdminRequests.setRequestType(RequestType.ALLOWED_COUNTRIES.name());


            CardControlAdminRequestDetail cardControlAdminRequestDetail = new CardControlAdminRequestDetail(  );

            controlAdminRequests.setRequestDetail(cardControlAdminRequestDetail);
            cardControlAdminRequestDetail.setAdminRequests(controlAdminRequests);
            cardControlAdminRequestDetail.setAllowedCountries(otherTrxnSearchRequest.getAllowedCountries());
            cardControlAdminRequestDetail.setCustID(otherTrxnSearchRequest.getCustID());
            cardControlAdminRequestDetail.setEncryptedFullPan(otherTrxnSearchRequest.getEncryptedFullPan());
            cardControlAdminRequestDetail.setMaskedPan(otherTrxnSearchRequest.getMaskedPan());
            cardControlAdminRequestDetail.setDailyLimit(otherTrxnSearchRequest.getDailyLimit());
            cardControlAdminRequestDetail.setMonthlyLimit(otherTrxnSearchRequest.getMonthlyLimit());

            this.adminRequestsService.saveRequest( controlAdminRequests);

            String message = "Card Settings.";
            return message;
        }



//    @Secured({"ROLE_ADMIN","ROLE_USER"})
//    @PostMapping(value="/app/otherTrxSearchByCustIdDate")
//    public List<OtherCardTransaction> processSearchByCustIdAndDate(@RequestBody() OtherTrxnSearchRequest otherTrxnSearchRequest,
//                                                                   HttpServletRequest httpServletRequest){
//
//        Date dateFrom = AdminUtil.strToDateConvert(otherTrxnSearchRequest.getSearchDateFrom());
//        Date dateTo = AdminUtil.strToDateConvert(otherTrxnSearchRequest.getSearchDateTo());
//
//        List<OtherCardTransaction> otherCardTransactionList = this.otherCardTransactionService.getOtherTransactionByCustIDDateRange(otherTrxnSearchRequest.getCustID(),dateFrom,dateTo);
//        return otherCardTransactionList;
//
//    }
}
