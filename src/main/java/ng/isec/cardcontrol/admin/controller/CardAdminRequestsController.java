package ng.isec.cardcontrol.admin.controller;

import ng.isec.cardcontrol.admin.dao.CardControlAdminRepository;
import ng.isec.cardcontrol.admin.dao.IUserInfoDAO;
import ng.isec.cardcontrol.admin.entity.*;
import ng.isec.cardcontrol.admin.service.AdminUserService;
import ng.isec.cardcontrol.admin.service.CardAdminRequestsService;
import ng.isec.cardcontrol.admin.util.AdminUtil;
import ng.isec.cardcontrol.admin.util.SSLUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@PropertySource(value="classpath:application.properties")
@ConfigurationProperties(value="api")
public class CardAdminRequestsController {

    private final Logger logger = LoggerFactory.getLogger(CardAdminRequestsController.class);

    @Value("${api.addCard}")
    private String addCard;

    @Value("${api.lockCard}")
    private String lockCard;

    @Value("${api.updateCountries}")
    private String updateCountries;

    @Value("${api.dailyLimit}")
    private String dailyLimit;

    @Value("${api.monthlyLimit}")
    private String monthlyLimit;

    @Value("${api.serverUrl}")
    private String serverUrl;

    @Value("${api.searchCard}")
    private String searchCardURL;

    @Value("${api.port}")
    private String serverPort;

    private final CardAdminRequestsService adminRequestsService;

    private final IUserInfoDAO userInfoDAO;

    private final CardControlAdminRepository controlAdminRepository;

    private final AdminUserService adminUserService;

    @Qualifier("searchRestTemplate")
    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    public CardAdminRequestsController(CardAdminRequestsService adminRequestsService,  IUserInfoDAO userInfoDAO, CardControlAdminRepository controlAdminRepository, AdminUserService adminUserService) {
        this.adminRequestsService = adminRequestsService;
//        this.passwordEncoder = passwordEncoder;
        this.userInfoDAO = userInfoDAO;
        this.controlAdminRepository = controlAdminRepository;
        this.adminUserService = adminUserService;
//        this. restTemplate = restTemplate;
    }

    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_CHECKER","ROLE_CONTROL_CHECKER"})
    @GetMapping(value = "/app/requests")
    public String showRequests(Principal principal, Model model,
                               HttpServletRequest httpServletRequest)
    {

        List<CardControlAdminRequests> adminRequests= this.adminRequestsService.getPendingRequest(principal.getName());

        model.addAttribute("adminRequests",adminRequests);
        return "adminRequests";
    }
    @Secured({"ROLE_ADMIN","ROLE_CONTACT_CENTER_CHECKER","ROLE_CONTROL_CHECKER"})
    @PostMapping(value = "/app/requests")
    public String processRequests(Principal principal,  HttpServletRequest httpServletRequest, @RequestParam("rqId") Integer requestId, @RequestParam("rqState") boolean status, Model model) throws KeyManagementException, NoSuchAlgorithmException {

        CardControlAdminRequests cardControlAdminRequest = this.adminRequestsService.getCardControlAdminRequest(requestId);

        if(status)
        {
            RequestType requestType = Enum.valueOf(RequestType.class, cardControlAdminRequest.getRequestType());

            switch (requestType)
            {
//                case EDIT_CARD:
//
//                    break;
//                case LOCK_CARD:
//                    ApiResponse apiResponse = this.restTemplate.postForObject(URI.create(serverUrl+lockCard),cardControlAdminRequest.getRequestDetail(), ApiResponse.class);
//                    String message = apiResponse.getMessage();
//                    this.approveRequest(principal,httpServletRequest,cardControlAdminRequest);
//                    return "redirect:/app/addCard?message="+message;
//
//                case DAILY_LIMIT:
//
//                apiResponse = this.restTemplate.postForObject(URI.create( serverUrl+dailyLimit),cardControlAdminRequest.getRequestDetail(), ApiResponse.class);
//                message = apiResponse.getMessage();
//                    this.approveRequest(principal,httpServletRequest,cardControlAdminRequest);
//
//                    return "redirect:/app/addCard?message="+message;
//                case MONTHLY_LIMIT:
//
//                apiResponse = this.restTemplate.postForObject(URI.create(serverUrl+ monthlyLimit),cardControlAdminRequest.getRequestDetail(), ApiResponse.class);
//                message = apiResponse.getMessage();
//                    this.approveRequest(principal,httpServletRequest,cardControlAdminRequest);
//
//                return "redirect:/app/addCard?message="+message;
                case CREATE_CARD:

                    ApiResponse apiResponse =  this.restTemplate.postForObject(URI.create(serverUrl+this.addCard),cardControlAdminRequest.getRequestDetail(), ApiResponse.class);
                    String message = apiResponse.getMessage();
                    this.approveRequest(principal,httpServletRequest,cardControlAdminRequest);

                    return "redirect:/app/addCard?message="+message;
                case ALLOWED_COUNTRIES:

                    CardControlAdminRequestDetail cardControlAdminRequestDetail = cardControlAdminRequest.getRequestDetail();
                    CardSettingsDTO cardSettingsDTO = new CardSettingsDTO();
                    cardSettingsDTO.setMonthlyLimit(BigDecimal.valueOf( cardControlAdminRequestDetail.getMonthlyLimit()));
                    cardSettingsDTO.setDailyLimit(BigDecimal.valueOf(cardControlAdminRequestDetail.getDailyLimit()));
                    cardSettingsDTO.setAllowedCountries(cardControlAdminRequestDetail.getAllowedCountries());
                    cardSettingsDTO.setBlocked(cardControlAdminRequestDetail.isBlocked());
                    logger.info("Invoking Controls Web Service");
                    SSLUtil.turnOffSslChecking();
                    apiResponse = this.restTemplate.postForObject(URI.create(serverUrl+this.addCard),cardSettingsDTO, ApiResponse.class);
                    logger.info("API Response: "+apiResponse.getMessage());
                    this.approveRequest(principal,httpServletRequest, cardControlAdminRequest);
                    return "redirect:/app/requests?success";
                case EDIT_USER:
//                    CardControlAdminUser cardControlAdminUser = this.controlAdminRepository.findOne(cardControlAdminRequest.getRequestDetail().getUserId());
                    Optional<CardControlAdminUser> cardControlAdminUser = this.adminUserService.getCardControlUser(cardControlAdminRequest.getRequestDetail().getUserId());
                    this.editUser(principal,httpServletRequest,cardControlAdminUser.get(),cardControlAdminRequest);
                    return "redirect:/app/requests?success";

                case CREATE_USER:
//                    CardControlAdminUser controlAdminUser = this.controlAdminRepository.findOne(cardControlAdminRequest.getRequestDetail().getUserId());
                    Optional<CardControlAdminUser> controlAdminUser = this.adminUserService.getCardControlUser(cardControlAdminRequest.getRequestDetail().getUserId());
                    this.createNewUser(principal,httpServletRequest,  controlAdminUser.get(),cardControlAdminRequest);
                    return "redirect:/app/requests?success";

            }

        }

        this.declineRequest(principal,httpServletRequest, cardControlAdminRequest,requestId);

//        cardControlAdminRequest.setId(requestId);
//        cardControlAdminRequest.setApproved(false);
//        this.adminRequestsService.saveRequest(cardControlAdminRequest);

        return "redirect:/app/requests?declined";
    }

    public void createNewUser(Principal principal, HttpServletRequest httpServletRequest, CardControlAdminUser controlAdminUser, CardControlAdminRequests cardControlAdminRequests) {
        CardControlAdminUser cardControlAdminUser = controlAdminUser;

        logger.info(""+ controlAdminUser.getRole());
        UserRoles role = Enum.valueOf(UserRoles.class,controlAdminUser.getRole());
        logger.info("Selected Role. "+ role.name());

        cardControlAdminUser.setId(controlAdminUser.getId());
        cardControlAdminUser.setRole((UserRoles.valueOf( controlAdminUser.getRole())).name());
        cardControlAdminUser.setUsername(controlAdminUser.getUsername());
        cardControlAdminUser.setEnabled(AdminUtil.USER_ENABLED.shortValue());
//        cardControlAdminUser.setPassword(this.passwordEncoder.encode(cardControlAdminUser.getPassword()));

//        this.controlAdminRepository.save(cardControlAdminUser);
        this.userInfoDAO.saveUser(cardControlAdminUser);

        cardControlAdminRequests.setId(cardControlAdminRequests.getId());
        cardControlAdminRequests.setApproved(true);
        cardControlAdminRequests.setTreated(true);
        this.adminRequestsService.saveRequest(cardControlAdminRequests);

    }

    public void editUser(Principal principal, HttpServletRequest httpServletRequest,CardControlAdminUser controlAdminUser,CardControlAdminRequests cardControlAdminRequests){

        controlAdminUser.setId(controlAdminUser.getId());
        controlAdminUser.setEnabled((cardControlAdminRequests.getRequestDetail().isEnabled() )? 1: 0);
        controlAdminUser.setRole((Enum.valueOf(UserRoles.class, controlAdminUser.getRole()).name()));

        this.controlAdminRepository.save(controlAdminUser);

        cardControlAdminRequests.setApproved(true);
        cardControlAdminRequests.setTreated(true);
        this.adminRequestsService.saveRequest(cardControlAdminRequests);

    }

    public void approveRequest(Principal principal, HttpServletRequest httpServletRequest, CardControlAdminRequests cardControlAdminRequests){
        cardControlAdminRequests.setApproved(true);
        cardControlAdminRequests.setTreated(true);
        this.adminRequestsService.saveRequest(cardControlAdminRequests);
    }

    public void declineRequest(Principal principal, HttpServletRequest httpServletRequest, CardControlAdminRequests cardControlAdminRequests, Integer reqId){
        cardControlAdminRequests.setId(reqId);
        cardControlAdminRequests.setApproved(false);
        cardControlAdminRequests.setTreated(true);
        this.adminRequestsService.saveRequest(cardControlAdminRequests);

    }

    public String getAddCard() {
        return addCard;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public String getServerPort() {
        return serverPort;
    }
}
