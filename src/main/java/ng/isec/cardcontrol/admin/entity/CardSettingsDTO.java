package ng.isec.cardcontrol.admin.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by isec on 09/01/2019.
 */
public class CardSettingsDTO extends CardControlSettings{

    private String AccountNo;
    private String[] mycountries;
    private String chosen;
    private String maskedPan;
    private String udk;

    protected List<Country> countriesList = new ArrayList<>();

    private List<Country> countriesSelected;

    private List<Channel> channelList;

    private List<String> isoCountriesList;

    private List<String> countryNames;

    private Map<String, List<Channel>> allowedCountriesChannelsMap;

    private CardControlAdminRequests cardControlAdminRequests;

    public List<Channel> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<Channel> channelList) {
        this.channelList = channelList;
    }

    public List<String> getIsoCountriesList() {
        return isoCountriesList;
    }

    public void setIsoCountriesList(List<String> isoCountriesList) {
        this.isoCountriesList = isoCountriesList;
    }

    public List<String> getCountryNames() {
        return countryNames;
    }

    public void setCountryNames(List<String> countryNames) {
        this.countryNames = countryNames;
    }

    public Map<String, List<Channel>> getAllowedCountriesChannelsMap() {
        return allowedCountriesChannelsMap;
    }

    public void setAllowedCountriesChannelsMap(Map<String, List<Channel>> allowedCountriesChannelsMap) {
        this.allowedCountriesChannelsMap = allowedCountriesChannelsMap;
    }

    public List<Country> getCountriesSelected() {
        return countriesSelected;
    }

    public void setCountriesSelected(List<Country> countriesSelected) {
        this.countriesSelected = countriesSelected;
    }

    public String getChosen() {
        return chosen;
    }

    public void setChosen(String chosen) {
        this.chosen = chosen;
    }

    public String[] getMycountries() {
        return mycountries;
    }

    public void setMycountries(String[] mycountries) {
        this.mycountries = mycountries;
    }

    public CardControlAdminRequests getCardControlAdminRequests() {
        return cardControlAdminRequests;
    }

    public void setCardControlAdminRequests(CardControlAdminRequests cardControlAdminRequests) {
        this.cardControlAdminRequests = cardControlAdminRequests;
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public void setAccountNo(String accountNo) {
        AccountNo = accountNo;
    }

    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    public String getUdk() {
        return udk;
    }

    public void setUdk(String udk) {
        this.udk = udk;
    }
}
