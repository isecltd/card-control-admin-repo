package ng.isec.cardcontrol.admin.entity;

public class Foo {
    private Long id;
    private String name;
    private String MaskedPan;
    private String EncryptedPan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaskedPan() {
        return MaskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        MaskedPan = maskedPan;
    }

    public String getEncryptedPan() {
        return EncryptedPan;
    }

    public void setEncryptedPan(String encryptedPan) {
        EncryptedPan = encryptedPan;
    }
}
