package ng.isec.cardcontrol.admin.entity;

public enum RequestType {

    CREATE_CARD("Set Up Card For Card Control"),
    EDIT_CARD("Edit Card Control Settings"),
    CREATE_USER("Create New Portal User"),
    LOCK_CARD("Lock Card For Transaction"),
    DAILY_LIMIT("Modify Card Daily Limit"),
    MONTHLY_LIMIT("Modify Card Monthly Limit"),
    ALLOWED_COUNTRIES("Modify/Add Card Control Settings."),
    EDIT_USER("Modify Current System User.");

    RequestType(String description) {
        value = description;
    }
    public String getRequestType(){
        return value;
    }

    private String value;

}
