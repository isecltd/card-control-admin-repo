package ng.isec.cardcontrol.admin.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;

@JsonDeserialize
//@JsonIgnoreProperties(ignoreUnknown = true)
public class CardDetail implements Serializable {
private String maskedPan;
private String encryptedPan;

    @JsonProperty("MaskedPan")
    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    @JsonProperty("EncryptedPan")
    public String getEncryptedPan() {
        return encryptedPan;
    }

    public void setEncryptedPan(String encryptedPan) {
        this.encryptedPan = encryptedPan;
    }
}
