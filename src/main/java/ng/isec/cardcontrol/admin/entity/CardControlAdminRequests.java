package ng.isec.cardcontrol.admin.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="ADMIN_REQUESTS")
public class CardControlAdminRequests  {

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name="STATUS")
    private boolean approved;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "adminRequests", cascade = CascadeType.ALL)
    private CardControlAdminRequestDetail requestDetail;

    @Column(name="DETAILS")
    private String requestDetails;

    @Column(name = "REQUEST_TYPE")
    private String requestType;

    @Column(name="INITIATOR")
    private String initiator;

    @Column(name="requestDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;

    @Column(name = "TREATED")
    private boolean treated;

    @Transient
    private RequestType typeOfRequest;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getRequestDetails() {
        return requestDetails;
    }

    public void setRequestDetails(String requestDetails) {
        this.requestDetails = requestDetails;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public CardControlAdminRequestDetail getRequestDetail() {
        return requestDetail;
    }

    public RequestType getTypeOfRequest() {
        typeOfRequest = Enum.valueOf(RequestType.class, getRequestType());
        return typeOfRequest;
    }

    public boolean isTreated() {
        return treated;
    }

    public void setTreated(boolean treated) {
        this.treated = treated;
    }

    public void setRequestDetail(CardControlAdminRequestDetail requestDetail) {
        this.requestDetail = requestDetail;
    }

    public CardControlAdminRequests() {
    }

    public CardControlAdminRequests(String requestType, String initiator, Date requestDate) {
        this.requestType = requestType;
        this.initiator = initiator;
        this.requestDate = requestDate;
    }
}
