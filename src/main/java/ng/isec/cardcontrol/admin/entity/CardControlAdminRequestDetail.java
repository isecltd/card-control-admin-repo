package ng.isec.cardcontrol.admin.entity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

@Entity
@Table(name="ADMIN_REQUEST_DETAIL")
public class CardControlAdminRequestDetail  {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id" ,referencedColumnName = "id")
    private CardControlAdminRequests adminRequests;

    @Column(name="ENCRYPTED_FULL_PAN", nullable = true)
    private String encryptedFullPan;

    @Column(name = "CUSTID", nullable = true)
    private String custID;

    @Column(name = "BLOCKED", nullable = true)
    private boolean blocked;

    @Column(nullable =true, name = "DAILY_LIMIT")
    private Double dailyLimit;

    @Column(name = "MONTHLY_LIMIT", nullable = true)
    private Double monthlyLimit;

    @Column(name = "ALLOWED_COUNTRIES", nullable = true)
    private String allowedCountries;

    @Column(name = "USERNAME",nullable = true)
    private String username;

    @Column(name = "USER_ENABLED", nullable = true)
    private boolean enabled;

    @Column(nullable = true, name = "USER_ROLE")
    private String userRole;

    @Column(name = "USER_ID",nullable = true)
    private Integer userId;

    @Column(name = "MASKED_PAN", nullable = true)
    private String maskedPan;

    @Transient
    private List<Country> countriesList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CardControlAdminRequests getAdminRequests() {
        return adminRequests;
    }

    public void setAdminRequests(CardControlAdminRequests adminRequests) {
        this.adminRequests = adminRequests;
    }

    public String getEncryptedFullPan() {
        return encryptedFullPan;
    }

    public void setEncryptedFullPan(String encryptedFullPan) {
        this.encryptedFullPan = encryptedFullPan;
    }

    public String getCustID() {
        return custID;
    }

    public void setCustID(String custID) {
        this.custID = custID;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public Double getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(Double dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public Double getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMonthlyLimit(Double monthlyLimit) {
        this.monthlyLimit = monthlyLimit;
    }

    public String getAllowedCountries() {
        return allowedCountries;
    }

    public void setAllowedCountries(String allowedCountries) {
        this.allowedCountries = allowedCountries;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }
    public List<Country> getCountriesList() {
        return countriesList;
    }
    public void setCountriesList(List<Country> countriesList) {
        this.countriesList = countriesList;
    }

    public void makeCountryList()
    {

        ObjectMapper objectMapper= new ObjectMapper();
        try {
            if(this.getAllowedCountries() != null )
            {
                List<Country>  countries = objectMapper.readValue(this.getAllowedCountries(), new TypeReference<List<Country>>(){});

                countries.stream().forEach((country) ->
                {
                    country.setCountryName( new Locale("", country.getTwoDigitCode()).getDisplayCountry());

                    country.getAllowedTransactionChannels().stream().forEach((channel -> {}));
                } );
                this.setCountriesList(countries);
            }


        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public CardControlAdminRequestDetail() {
    }

    public CardControlAdminRequestDetail(String encryptedFullPan, String custID, boolean blocked, Double dailyLimit, Double monthlyLimit, String allowedCountries, String username, boolean enabled, String userRole) {
        this.encryptedFullPan = encryptedFullPan;
        this.custID = custID;
        this.blocked = blocked;
        this.dailyLimit = dailyLimit;
        this.monthlyLimit = monthlyLimit;
        this.allowedCountries = allowedCountries;
        this.username = username;
        this.enabled = enabled;
        this.userRole = userRole;
    }
}
