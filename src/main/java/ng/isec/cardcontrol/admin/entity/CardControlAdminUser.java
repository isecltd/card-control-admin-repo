package ng.isec.cardcontrol.admin.entity;


import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created by isec on 20/02/2018.
 */
@Entity
@Table(name="card_admin_user")
public class CardControlAdminUser implements Serializable,UserDetails{

    @Id()
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @NotNull
    @NotEmpty(message = "Username must not be empty.")
    @Column(name="USERNAME")
    private String username;

    @NotNull
    @Column(name="PASSWORD")
    @NotEmpty(message = "Password must not be empty.")
    private String password;

    @NotNull
    @NotEmpty(message = "User Must Be Assigned A Role.")
    @Column(name="ROLE")
    private String role;

    @Column(name="ENABLED")
    private int enabled;

    @Transient
    private boolean accountNonLocked;

    @Transient
    private boolean accountNonExpired;

    @Transient
    private boolean credentialsNonExpired;

    @Transient
    private String enabledString = "DISABLED";

    @Transient
    private boolean accountEnabled;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getEnabledString() {

        if( this.getEnabled() == 1)
        {
            enabledString = "ENABLED";
        }
        return enabledString;
    }

    public boolean isAccountEnabled() {
//        accountEnabled = this.getEnabled() == 1? true: false;
        return accountEnabled;
    }

    public void setAccountEnabled(boolean accountEnabled) {
        this.accountEnabled = accountEnabled;
    }
}
