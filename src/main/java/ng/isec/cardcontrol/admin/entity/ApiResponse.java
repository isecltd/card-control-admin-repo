package ng.isec.cardcontrol.admin.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by isec on 10/01/2019.
 */
//@JsonDeserialize
    @JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponse {
    private String code;
    private String message;

    private CardDetail cardDetails;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CardDetail getCardDetails() {
        return cardDetails;
    }

    public void setCardDetails(CardDetail cardDetails) {
        this.cardDetails = cardDetails;
    }
}
