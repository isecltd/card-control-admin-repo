package ng.isec.cardcontrol.admin.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="CARDTRXNRESPONSELOG")
public class CardTransactionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(name="MTI")
	private String mti;
	@Column(name="MASKEDPAN")
	private String pan;

	@Column(name="ENCRYPTEDFULLPAN")
	private String encryptedFullPan;

	@Column(name="ACCTNUMBER")
	private String acctNumber;
	@Column(name="LOCATION")
	private String location;
	@Column(name="COUNTRY")
	private String country;
	@Column(name="RETRIEVALREFERENCENUMBER")
	private String rrnNumber;
	@Column(name="RESPONSECODE")
	private String responseCode;

	@Column(name="CURRENCYCODE")
	private String currencyCode;

	@Column(name="CHANNEL")
	private String channel;

	@Column(name="STAN")
	private String stan;
	@Column(name="TRNXTIME")
	private String trxTime;

	@Column(name="AMOUNT")
	private BigDecimal amount;

	@Temporal( value = TemporalType.DATE)
	@Column(name="UPDATEDDATE")
	private Date updatedDate;
	
	public String getMti() {
		return mti;
	}
	public void setMti(String mti) {
		this.mti = mti;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getEncryptedFullPan() {
		return encryptedFullPan;
	}

	public void setEncryptedFullPan(String encryptedFullPan) {
		this.encryptedFullPan = encryptedFullPan;
	}

	public String getAcctNumber() {
		return acctNumber;
	}
	public void setAcctNumber(String acctNumber) {
		this.acctNumber = acctNumber;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getRrnNumber() {
		return rrnNumber;
	}
	public void setRrnNumber(String rrnNumber) {
		this.rrnNumber = rrnNumber;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getTrxTime() {
		return trxTime;
	}
	public void setTrxTime(String trxTime) {
		this.trxTime = trxTime;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
}
