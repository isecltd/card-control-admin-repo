package ng.isec.cardcontrol.admin.config;

import java.net.URI;
import java.util.Arrays;
import java.util.Optional;

//import auth.AuthenticationServiceSoap;
import auth.UserAdDetails;
import ng.isec.cardcontrol.admin.dao.CardControlAdminRepository;
import ng.isec.cardcontrol.admin.dao.IUserInfoDAO;
import ng.isec.cardcontrol.admin.entity.*;
import ng.isec.cardcontrol.admin.util.AdminUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@ConfigurationProperties(value="ad")
public class AdminUserDetailsService implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(AdminUserDetailsService.class);

	@Autowired
	private IUserInfoDAO userInfoDAO;

//	@Autowired()
//	@Qualifier(value="restClient")
//	private RestTemplate adRestTemplate;

	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder passwordEncoder;

//	@Autowired()
//	@Qualifier("adAuthService")
//	private AuthenticationServiceSoap adServiceSoap;

	@Value("${ad.domain}")
	private String domain;

	@Value("${ad.reqHostCode}")
	private boolean reqHostCode;

	@Value("${ad.appKey}")
	private String appKey;

	@Value("${ad.appId}")
	private String appId;

	@Value("${ad.serverUrl}")
	private String adServerUrl;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {


		String credentialTokens[] = username.trim().split("\\|");
		username = credentialTokens[0];
		String password = credentialTokens[1];

		final String notFound = "The Username or password is incorrect";
//		validateUserADCredential(username,password);

//        UserAdDetails userAdDetails = this.adServiceSoap.getUserAdFullDetails(username, password, appId, appKey);
////        UserAdDetails userAdDetails = this.adServiceSoap.getUserAdFullDetails("oluwaseun.apata", "Ak4", "TestID", "087f7101089b76ce079e3725276d91d1a30872ba");
//        String userId = this.adServiceSoap.getUserAdStaffID(username, password, appId, appKey);
//        logger.info("AD User ID "+userId);
////
//        if (userAdDetails == null) {
//            throw new UsernameNotFoundException("Username not found!");
//
//        } else {
//
//			if (userId.contains(notFound)|| userId.contains("NA")) {
//				throw new UsernameNotFoundException("Username not found!");
//			}
//        }
//
//
//		if( userAdDetails.getGroups() == null)
//		{
//			throw new UsernameNotFoundException("Username not found!");
//		}
//
//		String adAuthResponse = userAdDetails.getGroups();
//
//		logger.info("Authenticate User Response: " + adAuthResponse);
//
////       UserAdDetails userAdDetails = this.adServiceSoap.getUserAdFullDetails(username,password, appId,appKey);
////
////       if(userAdDetails != null )
////       {
////           userAdDetails.getStaffName();
////       }
//
//		CardControlAdminUser newCardAdminUser = new CardControlAdminUser();
//		newCardAdminUser.setUsername(username);
//		newCardAdminUser.setPassword(this.passwordEncoder.encode(password));
//		newCardAdminUser.setRole(UserRoles.ROLE_ADMIN.name());
//		newCardAdminUser.setEnabled(AdminUtil.USER_ENABLED);
//
//		this.userInfoDAO.saveUser(newCardAdminUser);

//		UserInfo activeUserInfo = userInfoDAO.getActiveUser(username);
		CardControlAdminUser adminUser = userInfoDAO.getActiveCardAdminUser(username);

		if(adminUser == null )
		{
			throw new UsernameNotFoundException("Invalid username/Password.");

		}

		GrantedAuthority authority = new SimpleGrantedAuthority(adminUser.getRole());
		UserDetails userDetails = (UserDetails)new User(adminUser.getUsername(),
				adminUser.getPassword(), Arrays.asList(authority));
		return userDetails;
	}

//	private void validateUserADCredential(String username, String password){
//		AdRequest adRequest = new AdRequest();
//		adRequest.setAppId(appId);
//		adRequest.setAppKey(appKey);
//		adRequest.setLoginName(username);
//		adRequest.setPassword(password);
//
//		ApiResponse apiResponse = this.adRestTemplate.postForObject(URI.create(this.adServerUrl),adRequest,ApiResponse.class);
//
//	}



	public void setDomain(String domain) {
		this.domain = domain;
	}

	public void setReqHostCode(boolean reqHostCode) {
		this.reqHostCode = reqHostCode;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public void setAdServerUrl(String adServerUrl) {
		this.adServerUrl = adServerUrl;
	}
}

