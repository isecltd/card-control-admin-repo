package ng.isec.cardcontrol.admin.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private AdminUserDetailsService adminUserDetailsService;
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class).
				sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).
				maximumSessions(1).expiredUrl("/login?sessionExpired=true") .and().sessionFixation().migrateSession().and().
				authorizeRequests()
				.antMatchers("/login","/css/**",
						"/fonts/**",
						"/img/**",
						"/includes/**",
						"/js/**",
						"/styles/**",
						"/vendor/**").permitAll()
		.antMatchers("/app/**").hasAnyRole("ADMIN","USER","CONTROL_MAKER","CONTROL_CHECKER","CONTACT_CENTER_MAKER","CONTACT_CENTER_CHECKER","SUPPORT")
		.and().csrf().disable().formLogin()  //login configuration
        .loginPage("/login")
        .loginProcessingUrl("/app-login")
        .usernameParameter("app_username")
        .passwordParameter("app_password")
        .defaultSuccessUrl("/app/cardTrxn").
				failureUrl("/login?error=true")
		.and().logout()    //logout configuration
		.logoutUrl("/app-logout")
		.logoutSuccessUrl("/login?logout=true").
				invalidateHttpSession(true)
		.and().exceptionHandling() //exception handling configuration
		.accessDeniedPage("/app/error");
	}



//    @Autowired
//	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//    	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
////        auth.userDetailsService(adminUserDetailsService).passwordEncoder(passwordEncoder);
////		auth.authenticationProvider(authProvider());
//	}

	public SimpleAuthenticationFilter authenticationFilter() throws Exception {
		SimpleAuthenticationFilter filter = new SimpleAuthenticationFilter();
		filter.setAuthenticationManager(authenticationManagerBean());
		filter.setUsernameParameter("app_username");
		filter.setPasswordParameter("app_password");
		filter.setAuthenticationFailureHandler(failureHandler());
		filter.setAuthenticationSuccessHandler(new SimpleUrlAuthenticationSuccessHandler("/app/cardTrxn"));
		filter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/app-login","POST"));
		return filter;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider());
	}

	public AuthenticationProvider authProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(adminUserDetailsService);
		provider.setPasswordEncoder(passwordEncoder());
		return provider;
	}

	public SimpleUrlAuthenticationFailureHandler failureHandler() {
		return new SimpleUrlAuthenticationFailureHandler("/login?error=true");
	}

	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}