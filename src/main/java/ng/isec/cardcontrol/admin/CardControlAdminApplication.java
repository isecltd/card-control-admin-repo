package ng.isec.cardcontrol.admin;
import auth.AuthenticationService;
import auth.AuthenticationServiceSoap;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@SpringBootApplication
@EnableAspectJAutoProxy
public class CardControlAdminApplication {
	public static void main(String[] args) {
		SpringApplication.run(CardControlAdminApplication.class, args);
    }
//	@Autowired()
//	@Qualifier(value = "passwordEncoder")
//	private PasswordEncoder passwordEncoder;
//
//	@Bean
//	@Qualifier("cardControlRestTemplate")
//	public RestTemplate createRestTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
//		TrustStrategy trustStrategy = new TrustStrategy() {
//			@Override
//			public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//				return true;
//			}
//		};
//
//		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null,trustStrategy).build();
//		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,new NoopHostnameVerifier());
//		CloseableHttpClient closeableHttpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
//		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
//		requestFactory.setHttpClient(closeableHttpClient);
//
//		RestTemplate restTemplate = new RestTemplate(requestFactory);
//		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//		return restTemplate;
//	}
//
//	@Bean()
//	@Qualifier(value = "searchRestTemplate")
//	public RestTemplate createSearchRestTemplate(RestTemplateBuilder restTemplateBuilder){
//		RestTemplate restTemplate = restTemplateBuilder.build();
//		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//		return restTemplate;
//	}
//
//
//	@Bean()
//	@Qualifier(value="restClient")
//	public Client client(){
//		return ClientBuilder.newClient();
//}
//
//		@Bean("adAuthService")
//	public AuthenticationServiceSoap createADSOAPService()
//	{
//
//		final AuthenticationService authenticationService = new AuthenticationService();
//
//		final AuthenticationServiceSoap authServiceServiceSoap = authenticationService.getAuthenticationServiceSoap();
//		return authServiceServiceSoap;
//
//	}
//
//	@Bean("passwordEncoder")
//	public PasswordEncoder createPasswordEncoder()
//	{
//		return new BCryptPasswordEncoder();
//	}

}            