package ng.isec.cardcontrol.admin.dao;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ng.isec.cardcontrol.admin.entity.CardControlAdminUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ng.isec.cardcontrol.admin.entity.UserInfo;
@Repository
@Transactional
public class UserInfoDAO implements IUserInfoDAO {
	@PersistenceContext	
	private EntityManager entityManager;

//	public UserInfo getActiveUser(String userName) {
//		UserInfo activeUserInfo = new UserInfo();
//		short enabled = 1;
//		List<?> list = entityManager.createQuery("SELECT u FROM UserInfo u WHERE userName=? and enabled=?")
//				.setParameter(1, userName).setParameter(2, enabled).getResultList();
//		if(!list.isEmpty()) {
//			activeUserInfo = (UserInfo)list.get(0);
//		}
//		return activeUserInfo;
//	}

	@Override
	public CardControlAdminUser getActiveCardAdminUser(String username)
	{
		CardControlAdminUser cardControlAdminUser = null;

		Object obj = entityManager.createQuery("From CardControlAdminUser admUser where admUser.username= :username AND admUser.enabled=1").
				setParameter("username", username).
				getSingleResult();

		if(! (obj == null))
		{
			cardControlAdminUser = (CardControlAdminUser)obj;
		}
		return cardControlAdminUser;
	}

//	@SuppressWarnings("unchecked")
//	@Override
//	public List<Article> getAllUserArticles() {
//		String hql = "FROM Article as atcl ORDER BY atcl.articleId";
//		return (List<Article>) entityManager.createQuery(hql).getResultList();
//	}

	@Override
	public void saveUser(UserInfo userInfo) {
		entityManager.persist(userInfo);
	}

	@Override
	public void saveUser(CardControlAdminUser adminUser) {
		entityManager.persist(adminUser);
	}

}