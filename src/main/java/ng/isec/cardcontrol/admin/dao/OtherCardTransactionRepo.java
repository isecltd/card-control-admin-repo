package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.OtherCardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by isec on 12/12/2018.
 */
@Repository
public interface OtherCardTransactionRepo extends JpaRepository<OtherCardTransaction, Integer> {

    public List<OtherCardTransaction> findByPanOrderByUpdatedDateDesc(String pan);

    public List<OtherCardTransaction> findByUpdatedDateOrderByUpdatedDateDesc(Date updatedDate);

//    @Query(value = "select id, pan, acctNumber,amount,channel,country, location,terminalid,location,trnxtime,rrnnumber,stan,updateddate from othercardtranx where date(UPDATEDDATE) <= date(:toDate) and updateddate >= date(:fromDate)", nativeQuery = true)
//    public List<OtherCardTransaction> findByDateRange(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    @Query(value = "From OtherCardTransaction otherTrx where otherTrx.updatedDate Between :fromDate AND :toDate")
    public List<OtherCardTransaction> findByDateRange(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

//    @Query(value = "select id, pan, acctNumber,amount,channel,country, location,terminalid,location,trnxtime,rrnnumber,stan,updateddate from othercardtranx where (acctNumber =:acctNum) and date(UPDATEDDATE) <= date(:toDate) and updateddate >= date(:fromDate)", nativeQuery = true)
//    public List<OtherCardTransaction> findCustIdByDateRange( @Param("acctNum") String acctNumber,  @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    @Query(value = "From OtherCardTransaction otherTrx  where (otherTrx.acctNumber =:acctNum) and ( otherTrx.updatedDate Between :fromDate AND :toDate)")
    public List<OtherCardTransaction> findCustIdByDateRange( @Param("acctNum") String acctNumber,  @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);


}
