package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.CardControlAudit;
import ng.isec.cardcontrol.admin.entity.OtherCardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by isec on 13/06/2018.
 */

public interface CardControlAuditRepo extends JpaRepository<CardControlAudit, Long> {
//    @Query(value = "select * from AUDITRAIL order by updateddate desc limit 100",nativeQuery = true)
//    public List<CardControlAudit> findTopAll();

    @Query(value = "select top 100 * from AUDITRAIL order by updateddate desc",nativeQuery = true)
    public List<CardControlAudit> findTopAll();

//    @Query(value = "select id, custid, description,event,function_name,hostname, ip_address, updateddate,pan,username,rrnnumber from auditrail where date(UPDATEDDATE) <= date(:toDate) and updateddate >= date(:fromDate)", nativeQuery = true)
//    public List<CardControlAudit> findByDateRange(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);


    @Query(value = "From CardControlAudit audit where lastModifiedDate Between :fromDate AND :toDate")
    public List<CardControlAudit> findByDateRange(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    @Query("From CardControlAudit")
    Stream<CardControlAudit> streamAll();
}
