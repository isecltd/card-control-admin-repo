package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.CardControlAdminRequests;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardAdminRequestsRepository extends PagingAndSortingRepository<CardControlAdminRequests, Integer> {

//    @Query("Select adminRequests from CardControlAdminRequests as adminRequests Where adminRequests.approved = false order by adminRequests.id asc")
    @Query(value = "Select * from admin_requests where status = 0 order by ID DESC", nativeQuery = true)
    public List<CardControlAdminRequests> getPendingRequests();

    public List<CardControlAdminRequests> findByApprovedFalseOrderByIdDesc();

    public List<CardControlAdminRequests> findByApprovedFalseAndTreatedFalseAndInitiatorNot(String initiator , Sort sort);


}
