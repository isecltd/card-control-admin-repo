package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.CardControlSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CardControlSettingsRepository extends JpaRepository<CardControlSettings, Integer> {

	@Query(value = "select card from CardControlSettings card where card.pan like '%:pan'")
	public List<CardControlSettings> findByPanLike(@Param("pan") String pan);
	
	public CardControlSettings findByPan(String pan);

	public List<CardControlSettings> findByCustID(String custId);

	public List<CardControlSettings> findByCustIDAndPanLike(String custId, String pan );
}
