package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.CardControlAdminUser;
import ng.isec.cardcontrol.admin.entity.CardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface CardControlAdminRepository extends JpaRepository<CardControlAdminUser, Integer>{

//	@Query("From CardControlAdminUser admUser where admUser.username=? AND admUser.enabled=1")
//   public Optional<CardControlAdminUser> findAdminUser(String username);

	public Optional<CardControlAdminUser> findByUsername(String username);

	@Query("From CardControlAdminUser")
	Stream<CardControlAdminUser> streamAll();


}
