package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.CardControlAdminRequestDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRequestDetailRepo extends JpaRepository<CardControlAdminRequestDetail, Long> {

}
