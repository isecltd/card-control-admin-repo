package ng.isec.cardcontrol.admin.dao;
import java.util.List;

import ng.isec.cardcontrol.admin.entity.CardControlAdminUser;
import ng.isec.cardcontrol.admin.entity.UserInfo;
public interface IUserInfoDAO {
//	UserInfo getActiveUser(String userName);
//	List<Article> getAllUserArticles();

	void saveUser(UserInfo userInfo);
	void saveUser(CardControlAdminUser adminUser);
	public CardControlAdminUser getActiveCardAdminUser(String username);


}