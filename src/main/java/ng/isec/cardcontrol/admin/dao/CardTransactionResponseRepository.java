package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.CardTransaction;
import ng.isec.cardcontrol.admin.entity.CardTransactionResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.stream.Stream;

/**
 * Created by isec on 16/02/2018.
 */
public interface CardTransactionResponseRepository extends
        JpaRepository<CardTransactionResponse, Integer> {

    public CardTransactionResponse findByAcctNumberAndRrnNumberAndPanEndsWith(String acctNumber, String rrnNumber, String pan);

    public CardTransactionResponse findByRrnNumberAndStanAndPanEndsWith( String rrnNumber, String stan, String pan);

    public CardTransactionResponse findByRrnNumber( String rrnNumber);

    @Query("From CardTransactionResponse")
    Stream<CardTransactionResponse> streamAll();
}
