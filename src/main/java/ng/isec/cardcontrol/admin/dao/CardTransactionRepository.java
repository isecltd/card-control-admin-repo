package ng.isec.cardcontrol.admin.dao;

import ng.isec.cardcontrol.admin.entity.CardControlAudit;
import ng.isec.cardcontrol.admin.entity.CardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Stream;

public interface CardTransactionRepository extends
		JpaRepository<CardTransaction, Integer> {

	public List<CardTransaction> findByAcctNumberAndPanEndsWithOrderByUpdatedDateDesc(String acctNumber, String pan);

//	@Query(value = "select top 1000 * from cardtranx order by updatedDate desc",nativeQuery = true)
//	public List<CardTransaction> findTopAll();

	@Query(value = "select top 1000 * from cardtranx order by updatedDate desc",nativeQuery = true)
	public List<CardTransaction> findTopAll();

//	@Query(value = "select * from cardtranx order by updatedDate desc limit 1000",nativeQuery = true)
//	public List<CardTransaction> findTopAllLocal();

	@Query(value = "select top 1000 * from cardtranx order by updatedDate desc",nativeQuery = true)
	public List<CardTransaction> findTopAllLocal();

	@Query("From CardTransaction")
	Stream<CardTransaction> streamAll();

}
