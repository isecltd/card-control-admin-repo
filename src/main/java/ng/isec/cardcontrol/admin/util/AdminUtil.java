package ng.isec.cardcontrol.admin.util;

import ng.isec.cardcontrol.admin.entity.CardControlAdminUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * Created by isec on 08/03/2018.
 */
public class AdminUtil {

    public static final Integer USER_ENABLED = 1;

    public static final Integer USER_DISABLED = 0;

    public static Date strToDateConvert(String dateStr){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(dateStr,dateTimeFormatter);
        java.sql.Date search = java.sql.Date.valueOf(localDate);
        return  search;
    }
}
