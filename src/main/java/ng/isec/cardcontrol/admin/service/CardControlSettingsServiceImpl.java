package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.CardControlSettingsRepository;
import ng.isec.cardcontrol.admin.entity.CardControlSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardControlSettingsServiceImpl implements CardControlSettingsService {


    private final CardControlSettingsRepository cardControlSettingsRepository;

    @Autowired
    public CardControlSettingsServiceImpl(CardControlSettingsRepository cardControlSettingsRepository) {
        this.cardControlSettingsRepository = cardControlSettingsRepository;
    }

    @Override
    public List<CardControlSettings> findCardByCustId(String custId) {
        List<CardControlSettings> cardControlSettings = this.cardControlSettingsRepository.findByCustID(custId);
        return cardControlSettings;
    }

    @Override
    public List<CardControlSettings> findCardByCustIdAndPan(String custId, String maskedPan) {
        List<CardControlSettings> cardControlSettings = this.cardControlSettingsRepository.findByCustIDAndPanLike(custId,maskedPan);
        return cardControlSettings;
    }

}
