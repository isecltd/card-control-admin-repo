package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.entity.CardControlAdminRequestDetail;

public interface AdminRequestDetailService {

    public abstract void saveRequestDetail(CardControlAdminRequestDetail cardControlAdminRequestDetail);
}
