package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.entity.CardTransactionResponse;
import org.springframework.stereotype.Service;

public interface TransactionResponseService {

//    CardTransactionResponse getTransactionResponse(String acctNumber, String rrnNo, String pan);
    CardTransactionResponse getTransactionResponse(String rrnNo, String stan, String pan);
    CardTransactionResponse getTransactionResponse(String rrnNo);
}
