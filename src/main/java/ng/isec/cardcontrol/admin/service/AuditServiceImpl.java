package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.CardControlAuditRepo;
import ng.isec.cardcontrol.admin.entity.*;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@Aspect
public class AuditServiceImpl implements AuditService {

    private final CardControlAuditRepo cardControlAuditRepo;
    private final Logger logger = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Autowired
    public AuditServiceImpl(CardControlAuditRepo cardControlAuditRepo) {
        this.cardControlAuditRepo = cardControlAuditRepo;
    }

    @Override
    public List<CardControlAudit> getAllAudit() {
        List<CardControlAudit> cardControlAudits = this.cardControlAuditRepo.findTopAll();
        return cardControlAudits;
    }

    @Override
    public void saveAudit(CardControlAudit cardControlAudit) {
        this.cardControlAuditRepo.save(cardControlAudit);
    }

    @Override
    public List<CardControlAudit> getAuditByDateRange(Date fromDate, Date toDate) {
        List<CardControlAudit> auditList = this.cardControlAuditRepo.findByDateRange(fromDate,toDate);
        return auditList;
    }

    @After("execution(** ng.isec.cardcontrol.admin.controller.TransactionController.processViewCardSetting(..)), args(principal,..)")
    public void logViewCardSetting(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        Principal principal = (Principal) args[0];
        HttpServletRequest httpServletRequest = (HttpServletRequest)args[1];

        CardControlAudit cardControlAudit = new CardControlAudit();
        cardControlAudit.setIpAddress(httpServletRequest.getRemoteHost());
        cardControlAudit.setEvent("Viewed Card Control Setting");
        cardControlAudit.setUsername(principal.getName());
        cardControlAudit.setLastModifiedDate(Calendar.getInstance().getTime());

        this.saveAudit(cardControlAudit);

    }

    @After("execution(** ng.isec.cardcontrol.admin.controller.TransactionController.processNewUser(..)), args(principal,..)")
    public void logCreateNewUser(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        Principal principal = (Principal) args[0];
        HttpServletRequest httpServletRequest = (HttpServletRequest)args[1];
        CardControlAdminUser controlAdminUser = (CardControlAdminUser) args[2];

        CardControlAudit cardControlAudit = new CardControlAudit();
        cardControlAudit.setIpAddress(httpServletRequest.getLocalAddr());
        cardControlAudit.setEvent(RequestType.CREATE_USER.getRequestType()+" : "+controlAdminUser.getUsername());
        cardControlAudit.setUsername(principal.getName());
        cardControlAudit.setLastModifiedDate(Calendar.getInstance().getTime());

        this.saveAudit(cardControlAudit);
    }

    @After("execution(** ng.isec.cardcontrol.admin.controller.TransactionController.processEditUserRequest(..)), args(principal,..)")
    public void logEditUser(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        Principal principal = (Principal) args[0];
        HttpServletRequest httpServletRequest = (HttpServletRequest)args[1];
        CardControlAdminUser controlAdminUser = (CardControlAdminUser) args[2];

        CardControlAudit cardControlAudit = new CardControlAudit();
        cardControlAudit.setIpAddress(httpServletRequest.getLocalAddr());
        cardControlAudit.setEvent(RequestType.EDIT_USER.getRequestType()+" : "+controlAdminUser.getUsername());
        cardControlAudit.setUsername(principal.getName());
        cardControlAudit.setLastModifiedDate(Calendar.getInstance().getTime());

        this.saveAudit(cardControlAudit);
    }

    @After("execution(** ng.isec.cardcontrol.admin.controller.CardAdminRequestsController.approveRequest(..)), args(principal,..)")
    public void logRequestApproval(JoinPoint joinPoint){
        logger.info("Trying to log Request Approval");
        Object[] args = joinPoint.getArgs();
        Principal principal = (Principal) args[0];
        HttpServletRequest httpServletRequest = (HttpServletRequest)args[1];
        CardControlAdminRequests cardControlAdminRequests = (CardControlAdminRequests) args[2];
        logger.info("Logging Approval For"+cardControlAdminRequests.getRequestType());

        CardControlAudit cardControlAudit = new CardControlAudit();
        cardControlAudit.setIpAddress(httpServletRequest.getLocalAddr());
        cardControlAudit.setEvent("Approved "+cardControlAdminRequests.getRequestType()+" From "+cardControlAdminRequests.getInitiator());
        cardControlAudit.setUsername(principal.getName());
        cardControlAudit.setLastModifiedDate(Calendar.getInstance().getTime());

        this.saveAudit(cardControlAudit);
    }

    @After("execution(** ng.isec.cardcontrol.admin.controller.CardAdminRequestsController.declineRequest(..)),args(principal,..)")
    public void logRequestDeclination(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        Principal principal = (Principal) args[0];
        HttpServletRequest httpServletRequest = (HttpServletRequest)args[1];
        CardControlAdminRequests cardControlAdminRequests = (CardControlAdminRequests) args[2];

        CardControlAudit cardControlAudit = new CardControlAudit();
        cardControlAudit.setIpAddress(httpServletRequest.getLocalAddr());
        cardControlAudit.setEvent("Declined "+cardControlAdminRequests.getRequestType()+" From "+cardControlAdminRequests.getInitiator());
        cardControlAudit.setUsername(principal.getName());
        cardControlAudit.setLastModifiedDate(Calendar.getInstance().getTime());
        this.saveAudit(cardControlAudit);
    }

    @After("execution( ** ng.isec.cardcontrol.admin.controller.CardAdminRequestsController.editUser(..)),args(principal,..)")
    public void logUserEditApproval(JoinPoint joinPoint){
        logger.info("Trying to log Edit User Declination");

        Object[] args = joinPoint.getArgs();
        Principal principal = (Principal) args[0];
        HttpServletRequest httpServletRequest = (HttpServletRequest)args[1];
        CardControlAdminRequests cardControlAdminRequests = (CardControlAdminRequests)args[3];

        CardControlAudit cardControlAudit = new CardControlAudit();
        cardControlAudit.setIpAddress(httpServletRequest.getLocalAddr());
        cardControlAudit.setEvent("Declined "+cardControlAdminRequests.getRequestType()+" From "+cardControlAdminRequests.getInitiator());
        cardControlAudit.setUsername(principal.getName());
        cardControlAudit.setLastModifiedDate(Calendar.getInstance().getTime());
        this.saveAudit(cardControlAudit);
    }

//    @After("execution(** ng.isec.cardcontrol.admin.controller.CardAdminRequestsController.createNewUser(..)), args(principal,..)")
//    public void logNewUserApproval(JoinPoint joinPoint){
//
//    }

    @After("execution(** ng.isec.cardcontrol.admin.controller.RestSearchOtherTransactionController.processAllowedChannels(..)), args(principal,..)")
    public void logCardControlToggle(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        Principal principal = (Principal) args[0];
        OtherTrxnSearchRequest request = (OtherTrxnSearchRequest) args[1];

//        HttpServletRequest httpServletRequest = (HttpServletRequest)args[2];

        CardControlAudit cardControlAudit = new CardControlAudit();
//        cardControlAudit.setIpAddress(httpServletRequest.getLocalAddr());
        cardControlAudit.setEvent("Requesting Card Control For CUSTID: "+request.getCustID());
        cardControlAudit.setUsername(principal.getName());
        cardControlAudit.setLastModifiedDate(Calendar.getInstance().getTime());

        this.saveAudit(cardControlAudit);
    }
    @Pointcut("within(ng.isec.cardcontrol.admin.*)")
    private void definePointCut(){}

}
