package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.entity.CardControlAudit;
import ng.isec.cardcontrol.admin.entity.OtherCardTransaction;

import java.util.Date;
import java.util.List;

public interface AuditService {
    public List<CardControlAudit> getAllAudit();

    public void saveAudit(CardControlAudit cardControlAudit);

    public List<CardControlAudit> getAuditByDateRange( Date fromDate, Date toDate);

}
