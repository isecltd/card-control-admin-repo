package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.CardControlAdminRepository;
import ng.isec.cardcontrol.admin.entity.CardControlAdminUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardControlAdminUserServiceImpl implements AdminUserService {

    private final Logger logger = LoggerFactory.getLogger(CardTransactionServiceImpl.class);

    private final CardControlAdminRepository cardControlAdminRepository;
    @Autowired
    public CardControlAdminUserServiceImpl(CardControlAdminRepository cardControlAdminRepository) {
        this.cardControlAdminRepository = cardControlAdminRepository;
    }

    @Override
    public Optional<CardControlAdminUser> getCardControlUser(Integer id) {
        Optional<CardControlAdminUser> controlAdminUser = Optional.ofNullable(this.cardControlAdminRepository.getOne(id));
        return controlAdminUser;
    }

    @Override
    public List<CardControlAdminUser> getAllUsers() {
        List<CardControlAdminUser> cardControlAdminUsers= this.cardControlAdminRepository.findAll();
        return cardControlAdminUsers;
    }

    @Override
    public void saveUser(CardControlAdminUser controlAdminUser) {
            this.cardControlAdminRepository.save(controlAdminUser);
    }

    @Override
    public Optional<CardControlAdminUser> getCardControlUserByUsername(String username) {
        Optional<CardControlAdminUser> cardControlAdminUser = this.cardControlAdminRepository.findByUsername(username);
        return cardControlAdminUser;
    }
}
