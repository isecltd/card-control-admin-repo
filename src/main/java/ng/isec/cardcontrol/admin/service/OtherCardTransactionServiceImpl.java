package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.OtherCardTransactionRepo;
import ng.isec.cardcontrol.admin.entity.OtherCardTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

@Service
public class OtherCardTransactionServiceImpl implements OtherCardTransactionService{

    private final OtherCardTransactionRepo cardTransactionRepo;

    @Autowired
    public OtherCardTransactionServiceImpl(OtherCardTransactionRepo cardTransactionRepo) {
        this.cardTransactionRepo = cardTransactionRepo;
    }

    @Override
    public Stream<OtherCardTransaction> streamAll() {
        Stream<OtherCardTransaction> cardTransactionStream = cardTransactionRepo.findAll().stream();
        return cardTransactionStream;
    }

    @Override
    public List<OtherCardTransaction> getAllOtherTransactions() {
        List<OtherCardTransaction> cardTransactionList = this.cardTransactionRepo.findAll();
        return cardTransactionList;
    }

    @Override
    public List<OtherCardTransaction> getOtherTransactionByPan(String pan) {
        List<OtherCardTransaction> cardTransactionList = this.cardTransactionRepo.findByPanOrderByUpdatedDateDesc(pan);
        return cardTransactionList;
    }

    @Override
    public List<OtherCardTransaction> getOtherTransactionByUpdatedDate(Date date) {
        List<OtherCardTransaction> otherCardTransactionList = this.cardTransactionRepo.findByUpdatedDateOrderByUpdatedDateDesc(date);
        return otherCardTransactionList;
    }

    @Override
    public List<OtherCardTransaction> getOtherTransactionByDateRange(Date fromDate, Date toDate) {
        List<OtherCardTransaction> cardTransactionList = this.cardTransactionRepo.findByDateRange(fromDate,toDate);
        return cardTransactionList;
    }

    @Override
    public List<OtherCardTransaction> getOtherTransactionByCustIDDateRange(String acctNumber, Date fromDate, Date toDate) {
        List<OtherCardTransaction> cardTransactionList = this.cardTransactionRepo.findCustIdByDateRange(acctNumber,fromDate,toDate);
        return cardTransactionList;
    }

}
