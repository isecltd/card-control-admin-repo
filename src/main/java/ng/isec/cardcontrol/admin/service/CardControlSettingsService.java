package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.entity.CardControlSettings;

import java.util.List;

public interface CardControlSettingsService {

    public List<CardControlSettings> findCardByCustId(String custId);

    public List<CardControlSettings> findCardByCustIdAndPan(String custId, String maskedPan);
}
