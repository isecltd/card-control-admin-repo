package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.CardAdminRequestsRepository;
import ng.isec.cardcontrol.admin.entity.CardControlAdminRequests;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardAdminRequestsService {

    @Autowired
    private CardAdminRequestsRepository cardAdminRequestsRepository;



    public CardControlAdminRequests getCardControlAdminRequest(Integer id){
        CardControlAdminRequests cardControlAdminRequests = this.cardAdminRequestsRepository.findOne(id);

//        CardControlAdminRequests controlAdminRequests = cardControlAdminRequests.isPresent()? cardControlAdminRequests.get() : new CardControlAdminRequests();
        return cardControlAdminRequests;
    }

    public List<CardControlAdminRequests> getPendingRequests(){
        List<CardControlAdminRequests> adminRequestsList = this.cardAdminRequestsRepository.getPendingRequests();
        return adminRequestsList;
    }

    public List<CardControlAdminRequests> getPendingRequest(String username)
    {
        List<CardControlAdminRequests> adminRequestsList = this.cardAdminRequestsRepository.findByApprovedFalseAndTreatedFalseAndInitiatorNot(username,sort());
        return adminRequestsList;
    }

    public void saveRequest( CardControlAdminRequests adminRequests)
    {
        this.cardAdminRequestsRepository.save(adminRequests);
    }

    private Sort sort()
    {
       return new Sort(Sort.Direction.DESC,"requestDate");
    }

}
