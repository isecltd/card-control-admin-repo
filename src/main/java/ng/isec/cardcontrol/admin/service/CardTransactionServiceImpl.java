package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.CardTransactionRepository;
import ng.isec.cardcontrol.admin.entity.CardTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardTransactionServiceImpl implements CardTransactionService{

    @Autowired
    private final CardTransactionRepository transactionRepository;

    public CardTransactionServiceImpl(CardTransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public List<CardTransaction> getTopTransactions() {
        List<CardTransaction> cardTransactions = this.transactionRepository.findTopAll();
        return cardTransactions;
    }

    @Override
    public List<CardTransaction> getTopTrnx() {
        List<CardTransaction> cardTransactions=this.transactionRepository.findTopAllLocal();
        return cardTransactions;
    }

    @Override
    public List<CardTransaction> getCardTranx(String custId, String pan) {
        List<CardTransaction> cardTransactionList = this.transactionRepository.findByAcctNumberAndPanEndsWithOrderByUpdatedDateDesc(custId,pan);
        return cardTransactionList;
    }


}
