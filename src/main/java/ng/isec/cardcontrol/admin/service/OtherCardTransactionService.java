package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.entity.OtherCardTransaction;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public interface OtherCardTransactionService {

    public Stream<OtherCardTransaction> streamAll();

    public List<OtherCardTransaction> getAllOtherTransactions();

    public List<OtherCardTransaction> getOtherTransactionByPan(String pan);

    public List<OtherCardTransaction> getOtherTransactionByUpdatedDate(Date date);

    public List<OtherCardTransaction> getOtherTransactionByDateRange( Date fromDate, Date toDate);

    public List<OtherCardTransaction> getOtherTransactionByCustIDDateRange( String custID, Date fromDate, Date toDate);
}
