package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.CardTransactionResponseRepository;
import ng.isec.cardcontrol.admin.entity.CardTransactionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionResponseServiceImpl implements TransactionResponseService {

    private final CardTransactionResponseRepository transactionResponseRepository;

    @Autowired
    public TransactionResponseServiceImpl(CardTransactionResponseRepository transactionResponseRepository) {
        this.transactionResponseRepository = transactionResponseRepository;
    }

    @Override
    public CardTransactionResponse getTransactionResponse(String rrnNo, String stan, String pan) {
        CardTransactionResponse transactionResponse=this.transactionResponseRepository.findByRrnNumberAndStanAndPanEndsWith(rrnNo, stan,pan);
        return transactionResponse;
    }

    @Override
    public CardTransactionResponse getTransactionResponse(String rrnNo) {
        return transactionResponseRepository.findByRrnNumber(rrnNo);
    }
}
