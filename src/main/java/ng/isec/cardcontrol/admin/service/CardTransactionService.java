package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.entity.CardTransaction;

import java.util.List;

public interface CardTransactionService {

    public List<CardTransaction> getTopTransactions();

    public List<CardTransaction> getTopTrnx();

    public List<CardTransaction> getCardTranx(String custId, String pan);
}
