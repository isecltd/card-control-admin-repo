package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.entity.CardControlAdminUser;

import java.util.List;
import java.util.Optional;

public interface AdminUserService {

    public Optional<CardControlAdminUser> getCardControlUser(Integer integer);

    public List<CardControlAdminUser> getAllUsers();

    public void saveUser(CardControlAdminUser controlAdminUser);

    public Optional<CardControlAdminUser> getCardControlUserByUsername(String username);
}
