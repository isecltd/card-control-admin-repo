package ng.isec.cardcontrol.admin.service;

import ng.isec.cardcontrol.admin.dao.AdminRequestDetailRepo;
import ng.isec.cardcontrol.admin.entity.CardControlAdminRequestDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminRequestDetailServiceImpl implements AdminRequestDetailService {

    @Autowired
    private AdminRequestDetailRepo adminRequestDetailRepo;
    @Override
    public void saveRequestDetail(CardControlAdminRequestDetail cardControlAdminRequestDetail) {
        this.adminRequestDetailRepo.save(cardControlAdminRequestDetail);
    }
}
