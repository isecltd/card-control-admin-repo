
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserAdFullDetailsResult" type="{http://tempuri.org/}UserAdDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserAdFullDetailsResult"
})
@XmlRootElement(name = "GetUserAdFullDetailsResponse")
public class GetUserAdFullDetailsResponse {

    @XmlElement(name = "GetUserAdFullDetailsResult")
    protected UserAdDetails getUserAdFullDetailsResult;

    /**
     * Gets the value of the getUserAdFullDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link UserAdDetails }
     *     
     */
    public UserAdDetails getGetUserAdFullDetailsResult() {
        return getUserAdFullDetailsResult;
    }

    /**
     * Sets the value of the getUserAdFullDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserAdDetails }
     *     
     */
    public void setGetUserAdFullDetailsResult(UserAdDetails value) {
        this.getUserAdFullDetailsResult = value;
    }

}
