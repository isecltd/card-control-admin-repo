
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserAdGroupsResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserAdGroupsResult"
})
@XmlRootElement(name = "GetUserAdGroupsResponse")
public class GetUserAdGroupsResponse {

    @XmlElement(name = "GetUserAdGroupsResult")
    protected String getUserAdGroupsResult;

    /**
     * Gets the value of the getUserAdGroupsResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetUserAdGroupsResult() {
        return getUserAdGroupsResult;
    }

    /**
     * Sets the value of the getUserAdGroupsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetUserAdGroupsResult(String value) {
        this.getUserAdGroupsResult = value;
    }

}
