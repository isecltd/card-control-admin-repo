
package auth;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserFinacleDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserFinacleDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StaffID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FinacleUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RoleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GenericRoleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SOL_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserFinacleDetails", propOrder = {
    "staffID",
    "finacleUserID",
    "roleID",
    "branchCode",
    "genericRoleID",
    "soldesc"
})
public class UserFinacleDetails {

    @XmlElement(name = "StaffID")
    protected String staffID;
    @XmlElement(name = "FinacleUserID")
    protected String finacleUserID;
    @XmlElement(name = "RoleID")
    protected String roleID;
    @XmlElement(name = "BranchCode")
    protected String branchCode;
    @XmlElement(name = "GenericRoleID")
    protected String genericRoleID;
    @XmlElement(name = "SOL_DESC")
    protected String soldesc;

    /**
     * Gets the value of the staffID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStaffID() {
        return staffID;
    }

    /**
     * Sets the value of the staffID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStaffID(String value) {
        this.staffID = value;
    }

    /**
     * Gets the value of the finacleUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinacleUserID() {
        return finacleUserID;
    }

    /**
     * Sets the value of the finacleUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinacleUserID(String value) {
        this.finacleUserID = value;
    }

    /**
     * Gets the value of the roleID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleID() {
        return roleID;
    }

    /**
     * Sets the value of the roleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleID(String value) {
        this.roleID = value;
    }

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the genericRoleID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenericRoleID() {
        return genericRoleID;
    }

    /**
     * Sets the value of the genericRoleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenericRoleID(String value) {
        this.genericRoleID = value;
    }

    /**
     * Gets the value of the soldesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOLDESC() {
        return soldesc;
    }

    /**
     * Sets the value of the soldesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOLDESC(String value) {
        this.soldesc = value;
    }

}
