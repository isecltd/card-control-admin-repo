
package auth;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Branch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Branch">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BranchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ZoneCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ZoneCodeOps" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CsmEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BranchManagerEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Branch", propOrder = {
    "branchCode",
    "branchName",
    "zoneCode",
    "zoneCodeOps",
    "csmEmail",
    "branchManagerEmail"
})
public class Branch {

    @XmlElement(name = "BranchCode")
    protected String branchCode;
    @XmlElement(name = "BranchName")
    protected String branchName;
    @XmlElement(name = "ZoneCode")
    protected String zoneCode;
    @XmlElement(name = "ZoneCodeOps")
    protected String zoneCodeOps;
    @XmlElement(name = "CsmEmail")
    protected String csmEmail;
    @XmlElement(name = "BranchManagerEmail")
    protected String branchManagerEmail;

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the branchName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * Sets the value of the branchName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchName(String value) {
        this.branchName = value;
    }

    /**
     * Gets the value of the zoneCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZoneCode() {
        return zoneCode;
    }

    /**
     * Sets the value of the zoneCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZoneCode(String value) {
        this.zoneCode = value;
    }

    /**
     * Gets the value of the zoneCodeOps property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZoneCodeOps() {
        return zoneCodeOps;
    }

    /**
     * Sets the value of the zoneCodeOps property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZoneCodeOps(String value) {
        this.zoneCodeOps = value;
    }

    /**
     * Gets the value of the csmEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsmEmail() {
        return csmEmail;
    }

    /**
     * Sets the value of the csmEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsmEmail(String value) {
        this.csmEmail = value;
    }

    /**
     * Gets the value of the branchManagerEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchManagerEmail() {
        return branchManagerEmail;
    }

    /**
     * Sets the value of the branchManagerEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchManagerEmail(String value) {
        this.branchManagerEmail = value;
    }

}
