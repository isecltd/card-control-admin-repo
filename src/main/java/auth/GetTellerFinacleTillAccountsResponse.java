
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTellerFinacleTillAccountsResult" type="{http://tempuri.org/}ArrayOfTillAccount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTellerFinacleTillAccountsResult"
})
@XmlRootElement(name = "GetTellerFinacleTillAccountsResponse")
public class GetTellerFinacleTillAccountsResponse {

    @XmlElement(name = "GetTellerFinacleTillAccountsResult")
    protected ArrayOfTillAccount getTellerFinacleTillAccountsResult;

    /**
     * Gets the value of the getTellerFinacleTillAccountsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTillAccount }
     *     
     */
    public ArrayOfTillAccount getGetTellerFinacleTillAccountsResult() {
        return getTellerFinacleTillAccountsResult;
    }

    /**
     * Sets the value of the getTellerFinacleTillAccountsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTillAccount }
     *     
     */
    public void setGetTellerFinacleTillAccountsResult(ArrayOfTillAccount value) {
        this.getTellerFinacleTillAccountsResult = value;
    }

}
