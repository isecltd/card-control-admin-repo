
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetBranchResult" type="{http://tempuri.org/}ArrayOfBranch" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getBranchResult"
})
@XmlRootElement(name = "GetBranchResponse")
public class GetBranchResponse {

    @XmlElement(name = "GetBranchResult")
    protected ArrayOfBranch getBranchResult;

    /**
     * Gets the value of the getBranchResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBranch }
     *     
     */
    public ArrayOfBranch getGetBranchResult() {
        return getBranchResult;
    }

    /**
     * Sets the value of the getBranchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBranch }
     *     
     */
    public void setGetBranchResult(ArrayOfBranch value) {
        this.getBranchResult = value;
    }

}
