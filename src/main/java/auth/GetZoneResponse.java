
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetZoneResult" type="{http://tempuri.org/}ArrayOfZone" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getZoneResult"
})
@XmlRootElement(name = "GetZoneResponse")
public class GetZoneResponse {

    @XmlElement(name = "GetZoneResult")
    protected ArrayOfZone getZoneResult;

    /**
     * Gets the value of the getZoneResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfZone }
     *     
     */
    public ArrayOfZone getGetZoneResult() {
        return getZoneResult;
    }

    /**
     * Sets the value of the getZoneResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfZone }
     *     
     */
    public void setGetZoneResult(ArrayOfZone value) {
        this.getZoneResult = value;
    }

}
