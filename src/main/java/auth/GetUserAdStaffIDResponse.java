
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserAdStaffIDResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserAdStaffIDResult"
})
@XmlRootElement(name = "GetUserAdStaffIDResponse")
public class GetUserAdStaffIDResponse {

    @XmlElement(name = "GetUserAdStaffIDResult")
    protected String getUserAdStaffIDResult;

    /**
     * Gets the value of the getUserAdStaffIDResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetUserAdStaffIDResult() {
        return getUserAdStaffIDResult;
    }

    /**
     * Sets the value of the getUserAdStaffIDResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetUserAdStaffIDResult(String value) {
        this.getUserAdStaffIDResult = value;
    }

}
