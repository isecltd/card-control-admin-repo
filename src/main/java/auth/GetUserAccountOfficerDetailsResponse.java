
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserAccountOfficerDetailsResult" type="{http://tempuri.org/}UserAccountOfficerDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserAccountOfficerDetailsResult"
})
@XmlRootElement(name = "GetUserAccountOfficerDetailsResponse")
public class GetUserAccountOfficerDetailsResponse {

    @XmlElement(name = "GetUserAccountOfficerDetailsResult")
    protected UserAccountOfficerDetails getUserAccountOfficerDetailsResult;

    /**
     * Gets the value of the getUserAccountOfficerDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link UserAccountOfficerDetails }
     *     
     */
    public UserAccountOfficerDetails getGetUserAccountOfficerDetailsResult() {
        return getUserAccountOfficerDetailsResult;
    }

    /**
     * Sets the value of the getUserAccountOfficerDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserAccountOfficerDetails }
     *     
     */
    public void setGetUserAccountOfficerDetailsResult(UserAccountOfficerDetails value) {
        this.getUserAccountOfficerDetailsResult = value;
    }

}
