
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserFinacleDetailsResult" type="{http://tempuri.org/}UserFinacleDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserFinacleDetailsResult"
})
@XmlRootElement(name = "GetUserFinacleDetailsResponse")
public class GetUserFinacleDetailsResponse {

    @XmlElement(name = "GetUserFinacleDetailsResult")
    protected UserFinacleDetails getUserFinacleDetailsResult;

    /**
     * Gets the value of the getUserFinacleDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link UserFinacleDetails }
     *     
     */
    public UserFinacleDetails getGetUserFinacleDetailsResult() {
        return getUserFinacleDetailsResult;
    }

    /**
     * Sets the value of the getUserFinacleDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserFinacleDetails }
     *     
     */
    public void setGetUserFinacleDetailsResult(UserFinacleDetails value) {
        this.getUserFinacleDetailsResult = value;
    }

}
