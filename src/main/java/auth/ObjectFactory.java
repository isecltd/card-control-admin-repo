
package auth;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ad.auth package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ad.auth
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBranch }
     * 
     */
    public GetBranch createGetBranch() {
        return new GetBranch();
    }

    /**
     * Create an instance of {@link AuthenticateTokenResponse }
     * 
     */
    public AuthenticateTokenResponse createAuthenticateTokenResponse() {
        return new AuthenticateTokenResponse();
    }

    /**
     * Create an instance of {@link GetUserAdStaffID }
     * 
     */
    public GetUserAdStaffID createGetUserAdStaffID() {
        return new GetUserAdStaffID();
    }

    /**
     * Create an instance of {@link ResetToken }
     * 
     */
    public ResetToken createResetToken() {
        return new ResetToken();
    }

    /**
     * Create an instance of {@link GetZone }
     * 
     */
    public GetZone createGetZone() {
        return new GetZone();
    }

    /**
     * Create an instance of {@link GetStaffGroupWithUserNameResponse }
     * 
     */
    public GetStaffGroupWithUserNameResponse createGetStaffGroupWithUserNameResponse() {
        return new GetStaffGroupWithUserNameResponse();
    }

    /**
     * Create an instance of {@link GetUserAdFullDetails }
     * 
     */
    public GetUserAdFullDetails createGetUserAdFullDetails() {
        return new GetUserAdFullDetails();
    }

    /**
     * Create an instance of {@link GetStaffGroupWithUserName }
     * 
     */
    public GetStaffGroupWithUserName createGetStaffGroupWithUserName() {
        return new GetStaffGroupWithUserName();
    }

    /**
     * Create an instance of {@link GetStaffIDWithUserNameResponse }
     * 
     */
    public GetStaffIDWithUserNameResponse createGetStaffIDWithUserNameResponse() {
        return new GetStaffIDWithUserNameResponse();
    }

    /**
     * Create an instance of {@link GetUserAdFullDetailsResponse }
     * 
     */
    public GetUserAdFullDetailsResponse createGetUserAdFullDetailsResponse() {
        return new GetUserAdFullDetailsResponse();
    }

    /**
     * Create an instance of {@link UserAdDetails }
     * 
     */
    public UserAdDetails createUserAdDetails() {
        return new UserAdDetails();
    }

    /**
     * Create an instance of {@link ResetTokenResponse }
     * 
     */
    public ResetTokenResponse createResetTokenResponse() {
        return new ResetTokenResponse();
    }

    /**
     * Create an instance of {@link GetUserAccountOfficerDetailsResponse }
     * 
     */
    public GetUserAccountOfficerDetailsResponse createGetUserAccountOfficerDetailsResponse() {
        return new GetUserAccountOfficerDetailsResponse();
    }

    /**
     * Create an instance of {@link UserAccountOfficerDetails }
     * 
     */
    public UserAccountOfficerDetails createUserAccountOfficerDetails() {
        return new UserAccountOfficerDetails();
    }

    /**
     * Create an instance of {@link GetZoneResponse }
     * 
     */
    public GetZoneResponse createGetZoneResponse() {
        return new GetZoneResponse();
    }

    /**
     * Create an instance of {@link ArrayOfZone }
     * 
     */
    public ArrayOfZone createArrayOfZone() {
        return new ArrayOfZone();
    }

    /**
     * Create an instance of {@link GetUserAccountOfficerDetails }
     * 
     */
    public GetUserAccountOfficerDetails createGetUserAccountOfficerDetails() {
        return new GetUserAccountOfficerDetails();
    }

    /**
     * Create an instance of {@link GetUserDsaDetails }
     * 
     */
    public GetUserDsaDetails createGetUserDsaDetails() {
        return new GetUserDsaDetails();
    }

    /**
     * Create an instance of {@link GetUserAdStaffIDResponse }
     * 
     */
    public GetUserAdStaffIDResponse createGetUserAdStaffIDResponse() {
        return new GetUserAdStaffIDResponse();
    }

    /**
     * Create an instance of {@link GetUserAdGroupsResponse }
     * 
     */
    public GetUserAdGroupsResponse createGetUserAdGroupsResponse() {
        return new GetUserAdGroupsResponse();
    }

    /**
     * Create an instance of {@link GetUserFinacleDetailsResponse }
     * 
     */
    public GetUserFinacleDetailsResponse createGetUserFinacleDetailsResponse() {
        return new GetUserFinacleDetailsResponse();
    }

    /**
     * Create an instance of {@link UserFinacleDetails }
     * 
     */
    public UserFinacleDetails createUserFinacleDetails() {
        return new UserFinacleDetails();
    }

    /**
     * Create an instance of {@link GetUserDsaDetailsResponse }
     * 
     */
    public GetUserDsaDetailsResponse createGetUserDsaDetailsResponse() {
        return new GetUserDsaDetailsResponse();
    }

    /**
     * Create an instance of {@link UserDsaDetails }
     * 
     */
    public UserDsaDetails createUserDsaDetails() {
        return new UserDsaDetails();
    }

    /**
     * Create an instance of {@link AuthenticateToken }
     * 
     */
    public AuthenticateToken createAuthenticateToken() {
        return new AuthenticateToken();
    }

    /**
     * Create an instance of {@link GetStaffIDWithUserName }
     * 
     */
    public GetStaffIDWithUserName createGetStaffIDWithUserName() {
        return new GetStaffIDWithUserName();
    }

    /**
     * Create an instance of {@link GetBranchResponse }
     * 
     */
    public GetBranchResponse createGetBranchResponse() {
        return new GetBranchResponse();
    }

    /**
     * Create an instance of {@link ArrayOfBranch }
     * 
     */
    public ArrayOfBranch createArrayOfBranch() {
        return new ArrayOfBranch();
    }

    /**
     * Create an instance of {@link GetTellerFinacleTillAccounts }
     * 
     */
    public GetTellerFinacleTillAccounts createGetTellerFinacleTillAccounts() {
        return new GetTellerFinacleTillAccounts();
    }

    /**
     * Create an instance of {@link GetUserFinacleDetails }
     * 
     */
    public GetUserFinacleDetails createGetUserFinacleDetails() {
        return new GetUserFinacleDetails();
    }

    /**
     * Create an instance of {@link GetTellerFinacleTillAccountsResponse }
     * 
     */
    public GetTellerFinacleTillAccountsResponse createGetTellerFinacleTillAccountsResponse() {
        return new GetTellerFinacleTillAccountsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTillAccount }
     * 
     */
    public ArrayOfTillAccount createArrayOfTillAccount() {
        return new ArrayOfTillAccount();
    }

    /**
     * Create an instance of {@link GetUserAdGroups }
     * 
     */
    public GetUserAdGroups createGetUserAdGroups() {
        return new GetUserAdGroups();
    }

    /**
     * Create an instance of {@link Branch }
     * 
     */
    public Branch createBranch() {
        return new Branch();
    }

    /**
     * Create an instance of {@link Zone }
     * 
     */
    public Zone createZone() {
        return new Zone();
    }

    /**
     * Create an instance of {@link TillAccount }
     * 
     */
    public TillAccount createTillAccount() {
        return new TillAccount();
    }

}
