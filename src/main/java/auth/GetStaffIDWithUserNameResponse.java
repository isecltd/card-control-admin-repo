
package auth;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetStaffIDWithUserNameResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStaffIDWithUserNameResult"
})
@XmlRootElement(name = "GetStaffIDWithUserNameResponse")
public class GetStaffIDWithUserNameResponse {

    @XmlElement(name = "GetStaffIDWithUserNameResult")
    protected String getStaffIDWithUserNameResult;

    /**
     * Gets the value of the getStaffIDWithUserNameResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetStaffIDWithUserNameResult() {
        return getStaffIDWithUserNameResult;
    }

    /**
     * Sets the value of the getStaffIDWithUserNameResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetStaffIDWithUserNameResult(String value) {
        this.getStaffIDWithUserNameResult = value;
    }

}
