
$(document).ready(function(){

//    $('.allowed-countries').select2({placeholder:'Select a country...'});

    function addEventListeners(country) {
        var chosen = '<li data-country-code="'+country.id+'" data-country-channels=""><div class="row">'+
                         '<div class="col-md-6">'+
                             '<div>'+country.text+'</div>'+
                         '</div>'+
                         '<div class="col-md-6">'+
                             '<div class="pull-right">'+
                                 '<label class="checkbox-inline">POS'+
                                     '<input type="checkbox" class="option" guid="'+country.id+'" value="POS" name="POS"/>'+
                                     '</label>'+
                                     '<label class="checkbox-inline">ATM'+
                                            '<input type="checkbox" class="option" guid="'+country.id+'" value="ATM" name="ATM"/>'+
                                     '</label>'+
                                     '<label class="checkbox-inline">WEB'+
                                        '<input type="checkbox" class="option" guid="'+country.id+'" value="WEB" name="WEB"/>'+
                                 '</label>'+
                             '</div>'+
                         '</div>'+
                     '</div></li>';
                     chosen = $(chosen);
        var checkbox = chosen.find('.checkbox-inline');
        for (var i=0; i < checkbox.length; ++i) {
            $($(checkbox[i]).find('input')).click(function(e) {
                var d = ["ATM", "POS", "WEB"];
                var channels = [];
                for (var n = 0; n < d.length; ++n) {
                    if (chosen.find('[name="'+d[n]+'"]').is(':checked')) {
                        channels.push(d[n]);
                    }
                }
                $(chosen).attr('data-country-channels', channels.join(","));
            });
        }

       return chosen;
    }

    $('.option').change(function(){
    if(this.checked){
    var curr = $(this).attr('guid');
    console.log(curr);}
    });
        $('.allowed-countries').on('select2:select',function(e){
            $('.countriesLimit').append(addEventListeners(e.params.data));
        });


         $('.allowed-countries').on('select2:unselect',function(e){
                $('[data-country-code="'+e.params.data.id+'"]').remove();
         });

    var allowedChannelsReq = '';

     $('#users-trans').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            var data = {};
            data['maskedPan'] = $('#maskedPan').val();
            data['custId'] = $('#custId').val();
            data['lockCard'] = $('#lockCard').is(':checked');
            data['dailyLimit'] = parseFloat($('#dailyLimit').val());
            data['monthlyLimit'] = parseFloat($('#monthlyLimit').val());
            var allowedCountries = [];
            $('.countriesLimit').find('li').each(function(v, e) {
                console.log(e);
                var allowedChannels = []
                if ($(e).attr('data-country-channels')) {
                    allowedChannels = $(e).attr('data-country-channels').split(',')
                }
                allowedCountries.push({ [$(e).attr('data-country-code')]: allowedChannels });
            });
            data['allowedCountries'] = allowedCountries;

            //Submit
            console.log(data);
    var temp = data.allowedCountries;
    var finalList = [];
    temp.forEach(function (dat){
    var currentDat = new Object();
    var currentKey= (Object.keys(dat))[0];
    console.log(dat);
    console.log('current key: ' + currentKey);
    currentDat.allowedTransactionChannels = (Object.values(dat))[0];
    currentDat.twoDigitCode = currentKey;
    finalList.push(currentDat);
    })
            console.log(JSON.stringify(finalList));
            allowedChannelsReq = JSON.stringify(finalList);

            var custId = data.custId, maskPan = data.maskedPan;

            $.ajax(
                    {
                    type:"POST",
                    contentType:"application/json",
                    url:"/app/allowedCountries",
                    data:JSON.stringify({
                    allowedCountries:allowedChannelsReq,
                    custID:data.custId,
                    encryptedFullPan:data.maskedPan,
                    maskedPan: data.maskedPan,
                    dailyLimit: data.dailyLimit,
                    monthlyLimit:data.monthlyLimit
                    }
                    ),
                       dataType: 'json',
                       cache:false,
                       success:function(n){
                            console.log('Redirecting....');
    //                        $('#users-trans').attr('disable',true);
    //                        window.location.href=window.url+'/app/addCard';
                       }
                });
         });

     $('#allowed-countries-channel').multiselect({
                     enableFiltering: true,
                     maxHeight: 200,
                     dropRight: false,
                     filterPlaceholder: 'Search for something...',

                 onChange: function(element, checked) {
                     if (checked === true) {

                         $(".option ul li:last").after(

                         "<li class="+element.val()+" id=\"cardSettn\">"+
                         "<div class=\"row\">"+
                             "<div class=\"col-md-6\" id=\"country\">"+
                             "<div id="+element.val()+">"+element.val()+"</div>"+
                             "</div>"+
                             "<div class=\"col-md-6\" id=\"channel\">"+
                             "<div class=\"pull-right\" id=\"channels\">"+
                             "<label class=\"checkbox-inline\">"+
                             "<input type=\"checkbox\" id="+element.val()+"atm"+ " " +"value=\"ATM\"  name=\"channel\" /> ATM"+
                             "</label>"+
                             "<label class=\"checkbox-inline\">"+
                             "<input type=\"checkbox\" id="+element.val()+"pos"+ " " +"value=\"POS\"  name=\"channel\" /> POS"+
                             "</label>"+
                             "<label class=\"checkbox-inline\">"+
                             "<input type=\"checkbox\" id="+element.val()+"web"+ " " +"value=\"WEB\"  name=\"channel\" /> WEB"+
                             "</label>"+
                             "</div>"+
                             "</div>"+
                             "</div>"+
                             "</li>"
                         )
                     }

                     else if (checked === false) {

                             var data = '.'+element.val();

                             $(data).remove();

                     }
                 }

                 }

      );
});

$(document).on('click','#saveCard', function () {

    var li;
    var chan;
    var channel = [];
    var cardset = [];
    var cardSetting = [];
    var strTY = '{"allowedTransactionChannels":';
    var midStr = ',"twoDigitCode":';
    var endStr = "}";
    var constr;
    var strVal;
    var blocked = document.getElementById('locked').checked;
    var maskedPan = $('#maskedPan').val()
    var custId = $('#custId').val()
    var dailyLimitStr = $('#dailyLimit').val();
    var monthlyLimitStr = $('#monthlyLimit').val();
    var dailyLimit = parseFloat(dailyLimitStr);
    var monthlyLimit = parseFloat( monthlyLimitStr);

    $('li').each(function (index, value) {

        if($(this).attr('id') === "cardSettn"){
            channel = [];

            $(this).find("#country").each(function (index, element) {

               li =  JSON.stringify($(this).text());

               constr= li + endStr;

                console.log("Country selected ====>" + li);
            });

            $(this).find("#channels").each(function (index, elem) {

                $(this).find($("input[name='channel']:checked")).each(function (index, chkd) {

                    channel.push($(this).val());

                })

                var str = JSON.stringify(channel);

                strVal = strTY+str+midStr+constr

                cardSetting.push(strVal);

            });

        }
    });

    var finalCardSetting = "dailyLimit: "+dailyLimit+", monthlyLimit:"+monthlyLimit+",blocked: "+blocked+", allowedCountries:["+cardSetting.join(",")+"]"
    var allowedChannelsReq = "["+cardSetting.join(",")+"]";

    $('#allowed-countries-channel').multiselect('deselectAll', false);
    $('#allowed-countries-channel').multiselect('updateButtonText');

    $('#cardSettn').remove();
    console.log(finalCardSetting);

        $.ajax(
              {
               type:"POST",
               contentType:"application/json",
                url:"/app/allowedCountries",
                data:JSON.stringify({
                allowedCountries:allowedChannelsReq,
                custID:custId,
                encryptedFullPan: maskedPan,
                maskedPan: maskedPan,
                dailyLimit: dailyLimit,
                monthlyLimit: monthlyLimit
                }
               ),
               dataType: 'json',
                  cache:false,
                success:function(n){
                 console.log('Redirecting....');
                            $('#saveCard').attr('disable',true);
    //                        window.location.href=window.url+'/app/addCard';
               }
              });
         });

$(document).on('click','#editCard', function () {

    var li;
    var chan;
    var channel = [];
    var cardset = [];
    var cardSetting = [];
    var strTY = '{"allowedTransactionChannels":';
    var midStr = ',"twoDigitCode":';
    var endStr = "}";
    var constr;
    var strVal;
    var blocked = document.getElementById('locked').checked;
    var maskedPan = $('#maskedPan').val()
    var custId = $('#custId').val()
    var dailyLimitStr = $('#dailyLimit').val();
    var monthlyLimitStr = $('#monthlyLimit').val();
    var dailyLimit = parseFloat(dailyLimitStr);
    var monthlyLimit = parseFloat( monthlyLimitStr);

    $('li').each(function (index, value) {

        if($(this).attr('id') === "cardSettn"){
            channel = [];

            $(this).find("#country").each(function (index, element) {

               li =  JSON.stringify($(this).text());

               constr= li + endStr;

                console.log("Country selected ====>" + li);
            });

            $(this).find("#channels").each(function (index, elem) {

                $(this).find($("input[name='channel']:checked")).each(function (index, chkd) {

                    channel.push($(this).val());

                })

                var str = JSON.stringify(channel);

                strVal = strTY+str+midStr+constr

                cardSetting.push(strVal);

            });

        }
    });

    var finalCardSetting = "dailyLimit: "+dailyLimit+", monthlyLimit:"+monthlyLimit+",blocked: "+blocked+", allowedCountries:["+cardSetting.join(",")+"]"
    var allowedChannelsReq = "["+cardSetting.join(",")+"]";

    $('#allowed-countries-channel').multiselect('deselectAll', false);
    $('#allowed-countries-channel').multiselect('updateButtonText');

    $('#cardSettn').remove();
    console.log(finalCardSetting);

        $.ajax(
              {
               type:"POST",
               contentType:"application/json",
                url:"/app/allowedCountries",
                data:JSON.stringify({
                allowedCountries:allowedChannelsReq,
                custID:custId,
                encryptedFullPan: maskedPan,
                maskedPan: maskedPan,
                dailyLimit: dailyLimit,
                monthlyLimit: monthlyLimit
                }
               ),
               dataType: 'json',
                  cache:false,
                success:function(n){
                 console.log('Redirecting....');
                            $('#editCard').attr('disable',true);
    //                        window.location.href=window.url+'/app/addCard';
               }
              });
         });
