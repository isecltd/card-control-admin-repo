var eventBus = null;
var eventBusOpen = false;
var eventPath = 'http://localhost:8000/eventbus';
var eventAddress = 'verve-gateway.channel';

function initWS(){
    eventBus = new EventBus(eventPath);
    eventBus.onopen= function(){
        eventBusOpen = true;
        registerForMessages();
    }
    eventBus.onerror=function(err){
        eventBusOpen = false;
    }
}

function registerForMessages(){

    if(eventBusOpen)
    {
        eventBus.registerHandler(eventAddress,function(error, message){

            if(message)
            {
                var msg = message.body;

                if(msg == 'iSec VERVE Server Not Reachable.')
                {
                   $('#masterCardServer').css("background-color","red");
                }
                if(msg == "iSec VERVE Server Reachable.")
                {
                   $('#masterCardServer').css("background-color","green");

                }
                console.info(message);
            }
            else if(error)
            {
               console.error(error);
            }
        });
    }
    else
    {
        console.error("Could Not Register Messages. Event Bus Could Not Be Joined.");
    }
}



$(document).ready(function(){


    initWS();

    $('#requestsTable').DataTable();
    $('#cardTrxns').DataTable();
    $('#cardTrans').DataTable();
    $('#trxnResponseTable').DataTable();
    $('#allAudit').DataTable();

    $('#cardsTable').DataTable();
    $('#searchCardsTable').DataTable();

    $('#allUsers').DataTable();
    $('#allCardsTrans').DataTable();

//    $('.allowed-countries').select2({placeholder:'Select a country...'});

    function addEventListeners(country) {
        var chosen = '<li data-country-code="'+country.id+'" data-country-channels=""><div class="row">'+
                         '<div class="col-md-6">'+
                             '<div>'+country.text+'</div>'+
                         '</div>'+
                         '<div class="col-md-6">'+
                             '<div class="pull-right">'+
                                 '<label class="checkbox-inline">POS'+
                                     '<input type="checkbox" class="option" guid="'+country.id+'" value="POS" name="POS"/>'+
                                     '</label>'+
                                     '<label class="checkbox-inline">ATM'+
                                            '<input type="checkbox" class="option" guid="'+country.id+'" value="ATM" name="ATM"/>'+
                                     '</label>'+
                                     '<label class="checkbox-inline">WEB'+
                                        '<input type="checkbox" class="option" guid="'+country.id+'" value="WEB" name="WEB"/>'+
                                 '</label>'+
                             '</div>'+
                         '</div>'+
                     '</div></li>';
                     chosen = $(chosen);
        var checkbox = chosen.find('.checkbox-inline');
        for (var i=0; i < checkbox.length; ++i) {
            $($(checkbox[i]).find('input')).click(function(e) {
                var d = ["ATM", "POS", "WEB"];
                var channels = [];
                for (var n = 0; n < d.length; ++n) {
                    if (chosen.find('[name="'+d[n]+'"]').is(':checked')) {
                        channels.push(d[n]);
                    }
                }
                $(chosen).attr('data-country-channels', channels.join(","));
            });
        }

       return chosen;
    }

$('.option').change(function(){
if(this.checked){
var curr = $(this).attr('guid');
console.log(curr);}
});
    $('.allowed-countries').on('select2:select',function(e){
        $('.countriesLimit').append(addEventListeners(e.params.data));
    });


     $('.allowed-countries').on('select2:unselect',function(e){
            $('[data-country-code="'+e.params.data.id+'"]').remove();
     });

var allowedChannelsReq = '';

     $('#users-trans').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var data = {};
        data['maskedPan'] = $('#maskedPan').val();
        data['custId'] = $('#custId').val();
        data['lockCard'] = $('#lockCard').is(':checked');
        data['dailyLimit'] = parseFloat($('#dailyLimit').val());
        data['monthlyLimit'] = parseFloat($('#monthlyLimit').val());
        var allowedCountries = [];
        $('.countriesLimit').find('li').each(function(v, e) {
            console.log(e);
            var allowedChannels = []
            if ($(e).attr('data-country-channels')) {
                allowedChannels = $(e).attr('data-country-channels').split(',')
            }
            allowedCountries.push({ [$(e).attr('data-country-code')]: allowedChannels });
        });
        data['allowedCountries'] = allowedCountries;

        //Submit
        console.log(data);
var temp = data.allowedCountries;
var finalList = [];
temp.forEach(function (dat){
var currentDat = new Object();
var currentKey= (Object.keys(dat))[0];
console.log(dat);
console.log('current key: ' + currentKey);
currentDat.allowedTransactionChannels = (Object.values(dat))[0];
currentDat.twoDigitCode = currentKey;
finalList.push(currentDat);
})
        console.log(JSON.stringify(finalList));
        allowedChannelsReq = JSON.stringify(finalList);

        var custId = data.custId, maskPan = data.maskedPan;

        $.ajax(
                {
                type:"POST",
                contentType:"application/json",
                url:"/app/allowedCountries",
                data:JSON.stringify({
                allowedCountries:allowedChannelsReq,
                custID:data.custId,
                encryptedFullPan:data.maskedPan,
                maskedPan: data.maskedPan,
                dailyLimit: data.dailyLimit,
                monthlyLimit:data.monthlyLimit
                }
                ),
                   dataType: 'json',
                   cache:false,
                   success:function(n){
                        console.log('Redirecting....');
//                        $('#users-trans').attr('disable',true);
//                        window.location.href=window.url+'/app/addCard';
                   }
            });
     });


//                $('#allCardsTrans').DataTable(
//                                {
//                                "sAjaxSource":"/app/otherTrxSearch",
//                                "sAjaxDataProp": "",
//                      			"order": [[ 0, "asc" ]],
//                                "aoColumns": [
//                                                { mData: "pan" },
//                                                { mData: "acctNumber" },
//                                                { mData: "amount" },
//                                                { mData: "channel" },
//                                                { mData: "terminalID" },
//                                                { mData: "location" },
//                                                { mData: "trxDate" },
//                                                { mData: "rrnNumber" }
//                                            ]
//                                }
// );



});


$(document).on('click','#SearchAudit',function(){

    var fromDate = $('#dateFrom').val();
    var toDate = $('#dateTo').val();
     searchAuditByDate(fromDate,toDate);
});

function searchAuditByDate( frDate, tDate)
{

        $('#allAudit').DataTable(
         {
          responsive:true,
         "bDestroy":true,
          "ajax": { "url": "/app/auditByDate",
          "contentType":"application/json",
          "dataType": "json",
          "type": "POST" ,
          "cache":false,
          "data": function(d) {
                return JSON.stringify({ "searchDateFrom" : frDate,
                "searchDateTo" : tDate
                    })},
            "error": function(error){
                    console.log(error);
            }
         },
         "sAjaxDataProp": "",
         "dataSrc":""
         ,
          "aoColumns": [
          { data: "username", "autoWidth":true },
          { data: "custID", "autoWidth":true },
          { data: "pan" , "autoWidth":true},
          { data: "event" , "autoWidth":true},
          { data: "description" , "autoWidth":true},
          { data: "retrievalRefNumber" , "autoWidth":true},
          { data: "lastModifiedDate", "autoWidth":true, render : function(data, type, row){
                                                    return moment(row.lastModifiedDate).format('YYYY-MM-DD hh:mm:ss') ;
                                                }
          }
           ]
          }
         );

}


$(document).on('click', '#SearchTrxn',function(){

    var query = $('#searchField').val();
    var fr    = $('#dateFrom').val();
    var to    = $('#dateTo').val();
    var allCriteria = false;

    if(query != "" && fr != ""  && to !="" ){
            search_transaction_all(fr,to,query);
    }
    else
    {
      if( to != ""  && fr != ""  && query == "")
      {
          search_transaction_all(fr,to,"");
      }
         search_transaction_all(fr,to,query);

    }

    }
  );


function search_transaction_all(date1, date2, search){

          $('#allCardsTrans').DataTable(
                 {
                 "processing":true,
                 "bDestroy":true,
                  "ajax": { "url": "/app/otherTrxSearchByDate",
                  "contentType":"application/json",
                  "dataType": "json",
                  "type": "POST" ,
                  "cache":false,
                  "data": function(d) {
                        return JSON.stringify({ "searchDateFrom" : date1,
                        "searchDateTo" : date2, "custID": search
                            })},
                    "error": function(error){
                            console.log(error);
                    }
                 },
                 "sAjaxDataProp": "",
                 "dataSrc":""
                 ,
                  "aoColumns": [
                  { data: "pan", "autoWidth":true },
                  { data: "acctNumber", "autoWidth":true },
                  { data: "amount", "autoWidth":true },
                  { data: "channel" , "autoWidth":true},
                  { data: "terminalID" , "autoWidth":true},
                  { data: "location" , "autoWidth":true},
                  { data: "trxDate" , "autoWidth":true},
                  { data: "rrnNumber", "autoWidth":true, render : function(data, type, row){
                                                                 return '<a href=/app/viewTrxResponse?rrnNo='+data+'>View Response</a>';
                                                        }
                  }
                   ]
                  }
                 );

//
//        $.ajax({
//        type:"POST",
//        contentType:"application/json",
//        url:"/app/otherTrxSearchByDate",
//        data:JSON.stringify({
//        searchDateFrom:date1,
//        searchDateTo:date2,
//        custID:search
//        }),
//        dataType: 'json',
//        cache:false,
//        success:function(data){
//        console.log(data);
//
//        if(data!= null)
//        {
//          var t = $('#allCardsTrans').DataTable();
//
//          t.clear().draw();
//
//          data.forEach(function(current){
//          t.row.add([current.pan,current.acctNumber,current.amount, current.channel,current.terminalID, current.location, current.trxDate,current.rrnNumber]).draw();
//           });
//
////            $('#allCardsTrans').append("<tr><td>" + current.pan + "</td><td>" + current.acctNumber + "</td><td>"
////            + current.amount + "</td><td>"+ current.channel + "</td><td>"+ current.terminalID + "</td><td>"
////            + current.location + "</td><td>"+ current.trxDate + "</td><td>"+current.rrnNumber+"</td></tr>" );
////
////          });
//
//
////          for( var i =0; i < data.length; i++)
////          {
////             var panId = data[i].pan;
////                 acctNumber = data[i].acctNumber;
////                 amount = data[i].amount;
////                 channel = data[i].channel;
////                 terminalID = data[i].terminalID;
////                 location = data[i].location;
////                 trxDate = data[i].trxDate;
////                 rrnNumber = data[i].rrnNumber;
////
////                 t.row.add([panId,acctNumber,amount, channel,terminalID, location, trxDate,rrnNumber]).draw();
////
////          }
//        }
//        else
//        {
//            $('#allCardsTrans').context[0].oLanguage.sEmptyTable="No matching records found...";
//            $('#allCardsTrans').clear().draw();
//        }
//            console.log(data);
//        },error:function(jqXHR, textStatus, errorThrown ){
//            console.log(JSON.stringify(jqXHR));
//            console.log('AJAX error'+textStatus+': Error Thrown: '+errorThrown);
//            }
//    });

}
$(function() {
  		var self = $('.hover-popup');
      self.mouseover(function () {
        	self.next().children('.popup-box').fadeIn(350);
      });
      self.mouseout(function () {
        	self.next().children('.popup-box').fadeOut(350);
      });
});

function show(a){
    alert(a);
}





