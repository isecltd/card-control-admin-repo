$('#allCardsTrans').DataTable(
                                {
                                "sAjaxSource":"/app/otherTrxSearch",
                                "sAjaxDataProp": "",
                      			"order": [[ 0, "asc" ]],
                                "aoColumns": [
                                                { "mData": "pan" },
                                                { "mData": "acctNumber" },
                                                { "mData": "amount" },
                                                { "mData": "channel" },
                                                { "mData": "terminalID" },
                                                { "mData": "location" },
                                                { "mData": "trxDate" },
                                                { "mData": "rrnNumber" }
                                            ]
                                }
 );

$('#allowed-country-selected').multiselect({
    enableFiltering: true,
    maxHeight: 200,
    dropRight: false,
    filterPlaceholder: 'Search for country...',

}

);

});

$('.ui-datepicker').datepicker();

$( document ).ready(function() {
    
$('#cardTrans').DataTable();

$('#allCardsTrans').DataTable();



$('#logs').DataTable();

$('#example-multiple-selected').multiselect({
            enableFiltering: true,
            maxHeight: 200,
            dropRight: false,
            filterPlaceholder: 'Search for something...',

        onChange: function(element, checked) {
            if (checked === true) {

                $(".option ul li:last").after(

                "<li id="+element.val()+">"+
                "<div class=\"row\">"+
                    "<div class=\"col-md-6\">"+
                    "<div id="+element.val()+">"+element.val()+"</div>"+
                    "</div>"+
                    "<div class=\"col-md-6\">"+
                    "<div class=\"pull-right\">"+
                    "<label class=\"checkbox-inline\">"+
                    "<input type=\"checkbox\" id="+element.val()+"atm"+ " " +"value=\"ATM\" /> ATM"+
                    "</label>"+
                    "<label class=\"checkbox-inline\">"+
                    "<input type=\"checkbox\" id="+element.val()+"pos"+ " " +"value=\"POS\" /> POS"+
                    "</label>"+
                    "<label class=\"checkbox-inline\">"+
                    "<input type=\"checkbox\" id="+element.val()+"web"+ " " +"value=\"WEB\" /> WEB"+
                    "</label>"+
                    "</div>"+
                    "</div>"+
                    "</div>"+
                    "</li>"
                )
            }

            else if (checked === false) {

                    var data = '#'+element.val();

                    $(data).remove();

            }
        }

        }

    );


});



}
window.onload =function(){
 $('#allowed-country-selected').bind('click', function(){
     alert("Selected countries");

 });
}