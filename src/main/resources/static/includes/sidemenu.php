<!--======================================================================SIDEBAR===================================================================== -->

<div class="page-sidebar sidebar">
<div class="page-sidebar-inner slimscroll">

    <ul>

     <li class="link">
        <a href="dashboard.php">
         <!-- <span class="pe-7s-graph" aria-hidden="true"></span> -->
         <i class="pe-7s-graph"></i>
        <p class="linkTitle">Dashboard</p>
        </a>
     </li>

     <li class="link">
        <!-- <a href="#collapse-profile" data-toggle="collapse" aria-controls="collapse-profile"> -->
        <a href="bankCustomer.php" >
         <i class="pe-7s-user"></i>
         <p class="linkTitle">Profile</p>
        </a>
     </li>

     


     <li class="link">
            <a href="#collapse-transaction" data-toggle="collapse" aria-controls="collapse-transaction">
              <!-- <a href="transaction.html"  > -->
               <i class="pe-7s-repeat"></i>
               <p class="linkTitle">Transactions</p>
              </a>

              <ul class="collapse collapseable" id="collapse-transaction">
          <li>
            <a href="transaction.php">View All Transactions</a>
          </li>

          <li>
                <a href="userTransaction.php">View Users Transactions</a>
         </li>

        </ul>
              
           </li>

             


             <li class="link">
                <a href="createUser.php">
                 <i class="pe-7s-users"></i>
                 <p class="linkTitle">User</p>
                </a>
             </li>



             <li class="link">
             <a href="#collapse-profile" data-toggle="collapse" aria-controls="collapse-profile">
                <!-- <a href="resetUdk.html"> -->
                 <i class="pe-7s-phone"></i>
                 <p class="linkTitle">Mobile Device</p>
                </a>


            <ul class="collapse collapseable" id="collapse-profile">
            <li>
              <a href="resetUdk.php">Reset Udk</a>
            </li>

            <li>
                  <a href="resetPin.php">Reset Pin</a>
           </li>

          </ul>
             </li>



             <li class="link">
             <a href="#collapse-approval" data-toggle="collapse" aria-controls="collapse-approval">
               <!-- <a href="transaction.html"  > -->
                <i class="pe-7s-way"></i>
                <p class="linkTitle">Approval</p>
               </a>
 
               <ul class="collapse collapseable" id="collapse-approval">
           <li>
             <a href="profileApproval.php">Customer Profile</a>
           </li>
 
           <li>
                 <a href="resetApproval.php">All Reset</a>
          </li>
 
         </ul>
               
            </li>
           



             <li class="link">
                <a href="logs.php">
                 <i class="pe-7s-note2"></i>
                 <p class="linkTitle">Audit</p>
                </a>
             </li>



    </ul>
 
 
</div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->