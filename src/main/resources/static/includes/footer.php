<!-- ======================================================================FOOTER===================================================================== -->

<div class="page-footer">
<p class="no-s">Copyright &copy; i.Sec 2017</p>
</div>
</div><!-- Page Inner -->
</main><!-- Page Content -->

<div class="cd-overlay"></div>




<!-- Javascripts -->
<script src="vendor/jquery/jquery-2.1.4.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/jquery-blockui/jquery.blockui.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/datatables/js/jquery.datatables.min.js"></script>
<script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script> 
<script src="vendor/switchery/switchery.min.js"></script>
<script src="vendor/uniform/jquery.uniform.min.js"></script>
<script src="vendor/waves/waves.min.js"></script> 
<script src="vendor/3d-bold-navigation/js/main.js"></script>
<script src="js/modern.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/toastr.js"></script>
<script src="js/loading.js"></script>
<script src="js/Chart.bundle.js"></script>
<script src="js/utils.js"></script>
</body>
</html>