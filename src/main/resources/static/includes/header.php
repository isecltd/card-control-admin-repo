

<!DOCTYPE html>

<?php 

ob_start(); 


session_start();

include "libs/config.php";
include "libs/database.php";


if(!isset($_SESSION['email'])){
    
        header("Location: index.php");
        die();
    }
    

?>


<html>
    <head>
   
        <!-- Title -->
        <title>i.Sec Dashboard</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
       
        
        <!-- Styles -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'> -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>	
        <link href="assets/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" type="text/css"/>	
        <!-- Theme Styles -->
        <link href="css/modern.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/themes/green.css" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="css/toastr.css" rel="stylesheet" type="text/css"/>
        <link href="css/loading.css" rel="stylesheet" type="text/css"/>
        <link href="css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css"/>
        
        
       
        
    </head>
    <body class="page-header-fixed">
        <div class="overlay"></div>
   
        <form class="search-form" action="#" method="GET">
            <div class="input-group">
                <input type="text" name="search" class="form-control search-input" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default close-search" type="button"><i class="fa fa-times"></i></button>
                </span>
            </div><!-- Input Group -->
        </form><!-- Search Form -->
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="index.html" class="logo-text"><span>i.Sec</span></a>
                    </div><!-- Logo Box -->
                    <div class="search-button">
                        <a href="javascript:void(0);" class="show-search"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            
                            <ul class="nav navbar-nav navbar-right">
                                <li>	
                                    <a href="javascript:void(0);" class="show-search"><i class="fa fa-search"></i></a>
                                </li>
                                <!-- <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i><span class="badge badge-success pull-right">4</span></a>
                                   
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="badge badge-success pull-right">3</span></a>
                                   
                                </li> -->
                                <!-- http://lorempixel.com/400/200 -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class="user-name"><?php echo $_SESSION['email']; ?><i class="fa fa-angle-down"></i></span>
                                        <img class="img-circle avatar" src="" width="40" height="40" alt="">
                                    </a>
                                   
                                </li>
                                <li>
                                    <a href="logout.php" class="log-out">
                                        <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                                    </a>
                                </li>

                                <li>
                                    <div class="logo">
                                     <img src="img/providus_logo.png" />
                                    </div> 
                                 </li>
                                
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->